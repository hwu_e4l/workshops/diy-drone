EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_FET:LM5109AMA U?
U 1 1 5F7FF157
P 2500 1350
AR Path="/5F7FF157" Ref="U?"  Part="1" 
AR Path="/5F835A2C/5F7FF157" Ref="U?"  Part="1" 
AR Path="/601C7477/5F7FF157" Ref="U?"  Part="1" 
AR Path="/601CE793/5F7FF157" Ref="U?"  Part="1" 
AR Path="/601CE795/5F7FF157" Ref="U?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF157" Ref="U?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF157" Ref="U7"  Part="1" 
F 0 "U7" H 2500 1917 50  0000 C CNN
F 1 "LM5109AMA" H 2500 1826 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2500 850 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm5109a.pdf" H 2500 1350 50  0001 C CNN
	1    2500 1350
	1    0    0    -1  
$EndComp
Text Label 1900 1050 2    50   ~ 0
VDD
Text Label 1900 1650 2    50   ~ 0
VSS
Text Label 3150 1050 0    50   ~ 0
SUPPLY
Text Label 3150 1450 0    50   ~ 0
HIGH_SIDE_GATE
Text Label 3150 1650 0    50   ~ 0
LOW_SIDE_GATE
Text Label 3150 1550 0    50   ~ 0
SOURCE
Wire Wire Line
	2800 1050 3150 1050
Wire Wire Line
	3150 1450 2800 1450
Wire Wire Line
	2800 1550 3150 1550
Wire Wire Line
	3150 1650 2800 1650
Wire Wire Line
	6300 3550 6200 3550
Text GLabel 6300 3550 2    50   Input ~ 0
MOTOR_C_POS
Wire Wire Line
	6200 3550 6200 3600
Connection ~ 6200 3550
$Comp
L power:GND #PWR?
U 1 1 5F7C1F5E
P 6200 4000
AR Path="/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F5E" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F5E" Ref="#PWR0172"  Part="1" 
F 0 "#PWR0172" H 6200 3750 50  0001 C CNN
F 1 "GND" H 6200 3850 50  0000 C CNN
F 2 "" H 6200 4000 50  0000 C CNN
F 3 "" H 6200 4000 50  0000 C CNN
	1    6200 4000
	-1   0    0    -1  
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F7FF159
P 6200 3100
AR Path="/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF159" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF159" Ref="#PWR0173"  Part="1" 
F 0 "#PWR0173" H 6200 3000 50  0001 C CNN
F 1 "+VDC" H 6200 3375 50  0000 C CNN
F 2 "" H 6200 3100 50  0001 C CNN
F 3 "" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3500 6200 3550
Text Label 5050 3550 2    50   ~ 0
SOURCE
Text Label 5050 3800 2    50   ~ 0
LOW_SIDE_GATE
Text Label 5050 3300 2    50   ~ 0
HIGH_SIDE_GATE
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F7C1F6E
P 6100 3800
AR Path="/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F6E" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F6E" Ref="Q12"  Part="1" 
F 0 "Q12" H 6307 3846 50  0000 L CNN
F 1 "TIP122" H 6307 3755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6300 3725 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 6100 3800 50  0001 L CNN
	1    6100 3800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F7C1F74
P 6100 3300
AR Path="/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F74" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F74" Ref="Q11"  Part="1" 
F 0 "Q11" H 6307 3346 50  0000 L CNN
F 1 "TIP122" H 6307 3255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6300 3225 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 6100 3300 50  0001 L CNN
	1    6100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3550 6200 3550
Wire Wire Line
	1900 4950 1700 4950
Text Label 1700 4950 2    50   ~ 0
SUPPLY
$Comp
L Device:R_Small_US R?
U 1 1 5F7C1F7D
P 2550 4950
AR Path="/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/601C7477/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/601CE793/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/601CE795/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F7D" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F7D" Ref="R30"  Part="1" 
F 0 "R30" V 2755 4950 50  0000 C CNN
F 1 "1" V 2664 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2550 4950 50  0001 C CNN
F 3 "~" H 2550 4950 50  0001 C CNN
	1    2550 4950
	0    -1   -1   0   
$EndComp
Text Label 1150 1250 2    50   ~ 0
VSS
$Comp
L Device:C_Small C?
U 1 1 5F7C1F84
P 2200 6600
AR Path="/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/5F835A2C/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/601C7477/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/601CE793/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/601CE795/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F84" Ref="C?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F84" Ref="C30"  Part="1" 
F 0 "C30" V 2429 6600 50  0000 C CNN
F 1 "100 nF" V 2338 6600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 6600 50  0001 C CNN
F 3 "~" H 2200 6600 50  0001 C CNN
	1    2200 6600
	0    -1   -1   0   
$EndComp
Text Label 1600 6600 2    50   ~ 0
SUPPLY
Text Label 2850 6600 0    50   ~ 0
SOURCE
$Comp
L power:GND #PWR?
U 1 1 5F7C1F8C
P 1300 1250
AR Path="/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1F8C" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1F8C" Ref="#PWR0174"  Part="1" 
F 0 "#PWR0174" H 1300 1000 50  0001 C CNN
F 1 "GND" H 1300 1100 50  0000 C CNN
F 2 "" H 1300 1250 50  0000 C CNN
F 3 "" H 1300 1250 50  0000 C CNN
	1    1300 1250
	0    -1   1    0   
$EndComp
Wire Wire Line
	1600 6600 2100 6600
Wire Wire Line
	2300 6600 2850 6600
Text GLabel 1650 1450 0    50   Input ~ 0
HBRIDGE_C_HIGH_INPUT
Text GLabel 1650 1550 0    50   Input ~ 0
HBRIDGE_C_LOW_INPUT
Wire Wire Line
	1900 1050 2200 1050
Wire Wire Line
	1900 1650 2200 1650
Wire Wire Line
	1650 1450 2200 1450
Wire Wire Line
	1650 1550 2200 1550
Wire Wire Line
	5000 2050 5100 2050
Text GLabel 5000 2050 0    50   Input ~ 0
MOTOR_C_NEG
Wire Wire Line
	5100 2050 5100 2100
Connection ~ 5100 2050
Wire Wire Line
	5400 1800 5500 1800
Wire Wire Line
	5500 1500 5500 1800
Wire Wire Line
	5550 1500 5500 1500
$Comp
L Device:R_Small_US R?
U 1 1 5F7C1FA1
P 5700 1800
AR Path="/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/601C7477/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/601CE793/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/601CE795/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1FA1" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FA1" Ref="R33"  Part="1" 
F 0 "R33" V 5495 1800 50  0000 C CNN
F 1 "4.7" V 5586 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 1800 50  0001 C CNN
F 3 "~" H 5700 1800 50  0001 C CNN
	1    5700 1800
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7C1FA7
P 5100 2500
AR Path="/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1FA7" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FA7" Ref="#PWR0175"  Part="1" 
F 0 "#PWR0175" H 5100 2250 50  0001 C CNN
F 1 "GND" H 5100 2350 50  0000 C CNN
F 2 "" H 5100 2500 50  0000 C CNN
F 3 "" H 5100 2500 50  0000 C CNN
	1    5100 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F7FF161
P 5100 1600
AR Path="/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF161" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF161" Ref="#PWR0176"  Part="1" 
F 0 "#PWR0176" H 5100 1500 50  0001 C CNN
F 1 "+VDC" H 5100 1875 50  0000 C CNN
F 2 "" H 5100 1600 50  0001 C CNN
F 3 "" H 5100 1600 50  0001 C CNN
	1    5100 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 2000 5100 2050
Text Label 6250 2050 0    50   ~ 0
SOURCE
Text Label 6250 1800 0    50   ~ 0
LOW_SIDE_GATE
Text Label 6250 2300 0    50   ~ 0
HIGH_SIDE_GATE
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F7FF162
P 5200 2300
AR Path="/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF162" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF162" Ref="Q10"  Part="1" 
F 0 "Q10" H 5407 2346 50  0000 L CNN
F 1 "TIP122" H 5407 2255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 2225 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5200 2300 50  0001 L CNN
	1    5200 2300
	-1   0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F7FF163
P 5200 1800
AR Path="/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF163" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF163" Ref="Q9"  Part="1" 
F 0 "Q9" H 5407 1846 50  0000 L CNN
F 1 "TIP122" H 5407 1755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 1725 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5200 1800 50  0001 L CNN
	1    5200 1800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6250 2050 5100 2050
Text GLabel 5700 5100 0    50   Input ~ 0
MOTOR_C_POS
Text GLabel 5700 5350 0    50   Input ~ 0
MOTOR_C_NEG
$Comp
L Connector:TestPoint TP?
U 1 1 5F7FF164
P 5700 5100
AR Path="/602705E1/60274500/5F7FF164" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF164" Ref="TP11"  Part="1" 
F 0 "TP11" V 5654 5288 50  0000 L CNN
F 1 "TestPoint" V 5745 5288 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 5900 5100 50  0001 C CNN
F 3 "~" H 5900 5100 50  0001 C CNN
	1    5700 5100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5F7C1FCC
P 5700 5350
AR Path="/602705E1/60274500/5F7C1FCC" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FCC" Ref="TP12"  Part="1" 
F 0 "TP12" V 5654 5538 50  0000 L CNN
F 1 "TestPoint" V 5745 5538 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 5900 5350 50  0001 C CNN
F 3 "~" H 5900 5350 50  0001 C CNN
	1    5700 5350
	0    1    1    0   
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 5F7C1FD2
P 5450 5800
AR Path="/602705E1/60274500/5F7C1FD2" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FD2" Ref="D17"  Part="1" 
F 0 "D17" H 5443 5545 50  0000 C CNN
F 1 "LED_ALT" H 5443 5636 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5450 5800 50  0001 C CNN
F 3 "~" H 5450 5800 50  0001 C CNN
	1    5450 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5F7FF167
P 5950 5800
AR Path="/602705E1/60274500/5F7FF167" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF167" Ref="R35"  Part="1" 
F 0 "R35" V 5745 5800 50  0000 C CNN
F 1 "10k" V 5836 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 5800 50  0001 C CNN
F 3 "~" H 5950 5800 50  0001 C CNN
	1    5950 5800
	0    1    1    0   
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F7C1FDE
P 5050 5800
AR Path="/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1FDE" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FDE" Ref="#PWR0177"  Part="1" 
F 0 "#PWR0177" H 5050 5700 50  0001 C CNN
F 1 "+VDC" H 5050 6075 50  0000 C CNN
F 2 "" H 5050 5800 50  0001 C CNN
F 3 "" H 5050 5800 50  0001 C CNN
	1    5050 5800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7C1FE4
P 6300 5800
AR Path="/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7C1FE4" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C1FE4" Ref="#PWR0178"  Part="1" 
F 0 "#PWR0178" H 6300 5550 50  0001 C CNN
F 1 "GND" H 6300 5650 50  0000 C CNN
F 2 "" H 6300 5800 50  0000 C CNN
F 3 "" H 6300 5800 50  0000 C CNN
	1    6300 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 5800 5300 5800
Wire Wire Line
	5600 5800 5850 5800
Wire Wire Line
	6050 5800 6300 5800
Wire Notes Line
	4150 500  7150 500 
Wire Notes Line
	4150 4500 7150 4500
Wire Notes Line
	4150 4600 4150 6100
Text Notes 4200 4700 0    50   ~ 0
Status Flags
Wire Notes Line
	4700 4600 4700 4750
Wire Notes Line
	4700 4750 4150 4750
Wire Notes Line
	7150 500  7150 4500
Wire Notes Line
	4150 500  4150 4500
Text Notes 4200 600  0    50   ~ 0
H-Bridge 
Wire Notes Line
	4600 500  4600 650 
Wire Notes Line
	4600 650  4150 650 
Wire Notes Line
	4050 500  550  500 
Text Notes 1050 3400 0    50   ~ 0
The LM5109 is a  low cost high voltage gate driver,\ndesigned to drive both the high side and the low side\nN-Channel MOSFETs in a synchronous buck or a\nhalf bridge configuration. The floating high-side driver\nis capable of working with rail voltages up to 100V.\n\nThe outputs are independently controlled with TTL\ncompatible input thresholds. A robust level shifter\ntechnology operates at high speed while consuming\nlow power and providing clean level transitions from\nthe control input logic to the high side gate driver.\n\nUnder-voltage lockout is provided on both the low\nside and the high side power rails.
Text Notes 1050 2200 0    118  ~ 0
Description
Wire Notes Line
	3500 2000 1000 2000
Text Notes 1050 3700 0    50   ~ 0
Bootstrap Diode and Resistor
Text Label 2950 4950 0    50   ~ 0
VDD
Text Label 1100 1000 2    50   ~ 0
VDD
Text Notes 1100 6300 0    50   ~ 0
The bootstrap capacitor must maintain the voltage \ndifference between HB and HS voltage above the \nUVLO threshold for normal operation.\n\nNot gonna bother with the calculations as a \n100 nF capacitor is plenty enough to store the \ncharge required by a bootstrap cap.
Wire Notes Line
	1000 3600 3500 3600
$Comp
L power:+3.3V #PWR?
U 1 1 5F7C2001
P 1300 1000
AR Path="/602705E1/60274500/5F7C2001" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7C2001" Ref="#PWR0179"  Part="1" 
F 0 "#PWR0179" H 1300 850 50  0001 C CNN
F 1 "+3.3V" H 1315 1173 50  0000 C CNN
F 2 "" H 1300 1000 50  0001 C CNN
F 3 "" H 1300 1000 50  0001 C CNN
	1    1300 1000
	0    1    1    0   
$EndComp
Text Notes 1100 4750 0    50   ~ 0
Many half-bridge drivers incorporate a bootstrap\ndiode to generate high-side bias, reducing the \nboard space and component count. In high frequency\nand capacitive load applications, it is beneficial \nto add an external bootstrap diode to reduce losses.\n\nGeneral requirements:\n- Reverse recovery time of the bootstrap diode must \n  be very small, to reduce reverse recovery losses.\n- Low forward voltage drop (to reduce the size of \n  bootstrap capacitor).\n\n
$Comp
L Diode:1N4148WS D?
U 1 1 5F7FF16B
P 2050 4950
AR Path="/602705E1/60274500/5F7FF16B" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF16B" Ref="D16"  Part="1" 
F 0 "D16" H 2050 5167 50  0000 C CNN
F 1 "1N4148WS" H 2050 5076 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 2050 4775 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 2050 4950 50  0001 C CNN
	1    2050 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4950 2450 4950
Wire Wire Line
	2650 4950 2950 4950
Text Notes 1100 5450 0    50   ~ 0
The bootstrap diode selected has a forward drop \nof about 1.0 V and a maximum forward surge \ncurrent of 2 A.  The bootstrap resistor should \nreduce the inrush current to acceptable values. \n\n
Wire Notes Line
	1000 5450 3500 5450
Wire Notes Line
	3500 3600 3500 5450
Wire Notes Line
	1000 3600 1000 5450
Wire Notes Line
	1000 2000 1000 3500
Wire Notes Line
	1000 3500 3500 3500
Wire Notes Line
	3500 3500 3500 2000
Text Notes 1050 5650 0    50   ~ 0
Bootstrap and Bypass Capacitor
Wire Notes Line
	1000 5550 3500 5550
Wire Notes Line
	3500 5550 3500 7400
Wire Notes Line
	1000 5550 1000 7400
Text Label 1550 7200 2    50   ~ 0
VDD
Text Label 2850 7200 0    50   ~ 0
VSS
$Comp
L Device:C_Small C?
U 1 1 5F7C201D
P 2200 7200
AR Path="/5F7C201D" Ref="C?"  Part="1" 
AR Path="/5F835A2C/5F7C201D" Ref="C?"  Part="1" 
AR Path="/601C7477/5F7C201D" Ref="C?"  Part="1" 
AR Path="/601CE793/5F7C201D" Ref="C?"  Part="1" 
AR Path="/601CE795/5F7C201D" Ref="C?"  Part="1" 
AR Path="/602705E1/60274500/5F7C201D" Ref="C?"  Part="1" 
AR Path="/602705E1/60274504/5F7C201D" Ref="C31"  Part="1" 
F 0 "C31" V 2429 7200 50  0000 C CNN
F 1 "1 uF" V 2338 7200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 7200 50  0001 C CNN
F 3 "~" H 2200 7200 50  0001 C CNN
	1    2200 7200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 7200 2850 7200
Wire Wire Line
	1550 7200 2100 7200
Text Notes 1100 6950 0    50   ~ 0
The bypass capacitor filters any AC noise in a \nDC signal. Should be about ten times the bootstrap\ncapacitor value at least. 
Wire Notes Line
	1000 7400 3500 7400
Wire Notes Line
	1000 3750 2200 3750
Wire Notes Line
	2200 3750 2200 3600
Wire Notes Line
	1000 5700 2300 5700
Wire Notes Line
	2300 5700 2300 5550
Wire Wire Line
	1150 1250 1300 1250
Wire Wire Line
	1100 1000 1300 1000
$Comp
L Diode:1N4148WS D?
U 1 1 5F7FF16D
P 5700 1500
AR Path="/602705E1/60274500/5F7FF16D" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF16D" Ref="D20"  Part="1" 
F 0 "D20" H 5700 1283 50  0000 C CNN
F 1 "1N4148WS" H 5700 1374 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 1325 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5700 1500 50  0001 C CNN
	1    5700 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 1500 5900 1500
Wire Wire Line
	5900 1500 5900 1800
Wire Wire Line
	5900 1800 6250 1800
Wire Wire Line
	5500 1800 5600 1800
Connection ~ 5500 1800
Wire Wire Line
	5900 1800 5800 1800
Connection ~ 5900 1800
Wire Wire Line
	5400 2300 5500 2300
Wire Wire Line
	5500 2600 5500 2300
Wire Wire Line
	5550 2600 5500 2600
$Comp
L Device:R_Small_US R?
U 1 1 5F7FF16E
P 5700 2300
AR Path="/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/601C7477/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/601CE793/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/601CE795/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF16E" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF16E" Ref="R34"  Part="1" 
F 0 "R34" V 5495 2300 50  0000 C CNN
F 1 "4.7" V 5586 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 2300 50  0001 C CNN
F 3 "~" H 5700 2300 50  0001 C CNN
	1    5700 2300
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F7FF16F
P 5700 2600
AR Path="/602705E1/60274500/5F7FF16F" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF16F" Ref="D21"  Part="1" 
F 0 "D21" H 5700 2817 50  0000 C CNN
F 1 "1N4148WS" H 5700 2726 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 2425 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5700 2600 50  0001 C CNN
	1    5700 2600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 2600 5900 2600
Wire Wire Line
	5900 2600 5900 2300
Wire Wire Line
	5900 2300 6250 2300
Wire Wire Line
	5500 2300 5600 2300
Connection ~ 5500 2300
Wire Wire Line
	5900 2300 5800 2300
Connection ~ 5900 2300
Wire Wire Line
	5900 3300 5800 3300
Wire Wire Line
	5800 3000 5800 3300
Wire Wire Line
	5750 3000 5800 3000
$Comp
L Device:R_Small_US R?
U 1 1 5F7FF170
P 5600 3300
AR Path="/5F7FF170" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F7FF170" Ref="R?"  Part="1" 
AR Path="/601C7477/5F7FF170" Ref="R?"  Part="1" 
AR Path="/601CE793/5F7FF170" Ref="R?"  Part="1" 
AR Path="/601CE795/5F7FF170" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF170" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF170" Ref="R31"  Part="1" 
F 0 "R31" V 5395 3300 50  0000 C CNN
F 1 "4.7" V 5486 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3300 50  0001 C CNN
F 3 "~" H 5600 3300 50  0001 C CNN
	1    5600 3300
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F7C2059
P 5600 3000
AR Path="/602705E1/60274500/5F7C2059" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7C2059" Ref="D18"  Part="1" 
F 0 "D18" H 5600 2783 50  0000 C CNN
F 1 "1N4148WS" H 5600 2874 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5600 2825 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5600 3000 50  0001 C CNN
	1    5600 3000
	1    0    0    1   
$EndComp
Wire Wire Line
	5450 3000 5400 3000
Wire Wire Line
	5400 3000 5400 3300
Wire Wire Line
	5400 3300 5050 3300
Wire Wire Line
	5800 3300 5700 3300
Connection ~ 5800 3300
Wire Wire Line
	5400 3300 5500 3300
Connection ~ 5400 3300
Wire Wire Line
	5900 3800 5800 3800
Wire Wire Line
	5800 4100 5800 3800
Wire Wire Line
	5750 4100 5800 4100
$Comp
L Device:R_Small_US R?
U 1 1 5F7FF172
P 5600 3800
AR Path="/5F7FF172" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F7FF172" Ref="R?"  Part="1" 
AR Path="/601C7477/5F7FF172" Ref="R?"  Part="1" 
AR Path="/601CE793/5F7FF172" Ref="R?"  Part="1" 
AR Path="/601CE795/5F7FF172" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F7FF172" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF172" Ref="R32"  Part="1" 
F 0 "R32" V 5395 3800 50  0000 C CNN
F 1 "4.7" V 5486 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3800 50  0001 C CNN
F 3 "~" H 5600 3800 50  0001 C CNN
	1    5600 3800
	0    -1   1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F7FF173
P 5600 4100
AR Path="/602705E1/60274500/5F7FF173" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7FF173" Ref="D19"  Part="1" 
F 0 "D19" H 5600 4317 50  0000 C CNN
F 1 "1N4148WS" H 5600 4226 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5600 3925 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5600 4100 50  0001 C CNN
	1    5600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4100 5400 4100
Wire Wire Line
	5400 4100 5400 3800
Wire Wire Line
	5400 3800 5050 3800
Wire Wire Line
	5800 3800 5700 3800
Connection ~ 5800 3800
Wire Wire Line
	5400 3800 5500 3800
Connection ~ 5400 3800
Wire Notes Line
	4050 500  4050 7700
Wire Notes Line
	550  500  550  7700
Text Notes 600  600  0    50   ~ 0
H-Bridge Driver
Wire Notes Line
	550  650  1250 650 
Wire Notes Line
	1250 650  1250 500 
Wire Notes Line
	7150 4600 7150 6100
Wire Notes Line
	4150 6100 7150 6100
Wire Notes Line
	4150 4600 7150 4600
Wire Notes Line
	550  7700 4050 7700
$EndSCHEMATC
