EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5827 8268
encoding utf-8
Sheet 1 14
Title "Drone PCB"
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3850 1150 0    60   ~ 0
Reset
Text Label 3850 1350 0    60   ~ 0
2
Text Label 3850 1450 0    60   ~ 0
3(**)
Text Label 3850 1550 0    60   ~ 0
4
Text Label 3850 1650 0    60   ~ 0
5(**)
Text Label 3850 1750 0    60   ~ 0
6(**)
Text Label 3850 1850 0    60   ~ 0
7
Text Label 3850 1950 0    60   ~ 0
8
Text Label 3850 2050 0    60   ~ 0
9(**)
Text Label 4100 3700 2    60   ~ 0
A0
Text Label 4100 3600 2    60   ~ 0
A1
Text Label 4100 3500 2    60   ~ 0
A2
Text Label 4100 3400 2    60   ~ 0
A3
Text Label 4100 3300 2    60   ~ 0
A4
Text Label 4100 3200 2    60   ~ 0
A5
Text Label 4100 3100 2    60   ~ 0
A6
Text Label 4100 3000 2    60   ~ 0
A7
Text Label 4100 3800 2    60   ~ 0
AREF
Text Label 4100 2800 2    60   ~ 0
Reset
Text Notes 3750 3700 2    60   ~ 0
Holes
Text Label 4400 2600 2    60   ~ 0
Vin
$Comp
L Connector_Generic:Conn_01x01 P3
U 1 1 56D73ADD
P 3750 3350
F 0 "P3" V 3850 3350 50  0000 C CNN
F 1 "CONN_01X01" V 3850 3350 50  0001 C CNN
F 2 "Socket_Arduino_Nano:1pin_Nano" H 3750 3350 50  0001 C CNN
F 3 "" H 3750 3350 50  0000 C CNN
	1    3750 3350
	0    1    -1   0   
$EndComp
NoConn ~ 3750 3550
$Comp
L Connector_Generic:Conn_01x01 P4
U 1 1 56D73D86
P 3650 3350
F 0 "P4" V 3750 3350 50  0000 C CNN
F 1 "CONN_01X01" V 3750 3350 50  0001 C CNN
F 2 "Socket_Arduino_Nano:1pin_Nano" H 3650 3350 50  0001 C CNN
F 3 "" H 3650 3350 50  0000 C CNN
	1    3650 3350
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P5
U 1 1 56D73DAE
P 3550 3350
F 0 "P5" V 3650 3350 50  0000 C CNN
F 1 "CONN_01X01" V 3650 3350 50  0001 C CNN
F 2 "Socket_Arduino_Nano:1pin_Nano" H 3550 3350 50  0001 C CNN
F 3 "" H 3550 3350 50  0000 C CNN
	1    3550 3350
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 P6
U 1 1 56D73DD9
P 3450 3350
F 0 "P6" V 3550 3350 50  0000 C CNN
F 1 "CONN_01X01" V 3550 3350 50  0001 C CNN
F 2 "Socket_Arduino_Nano:1pin_Nano" H 3450 3350 50  0001 C CNN
F 3 "" H 3450 3350 50  0000 C CNN
	1    3450 3350
	0    1    -1   0   
$EndComp
NoConn ~ 3650 3550
NoConn ~ 3550 3550
NoConn ~ 3450 3550
$Comp
L Connector_Generic:Conn_01x15 P1
U 1 1 56D73FAC
P 4700 1650
F 0 "P1" H 4700 2450 50  0000 C CNN
F 1 "Digital" V 4800 1650 50  0000 C CNN
F 2 "Socket_Arduino_Nano:Socket_Strip_Arduino_1x15" H 4700 1650 50  0001 C CNN
F 3 "" H 4700 1650 50  0000 C CNN
	1    4700 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x15 P2
U 1 1 56D740C7
P 4700 3300
F 0 "P2" H 4700 4100 50  0000 C CNN
F 1 "Analog" V 4800 3300 50  0000 C CNN
F 2 "Socket_Arduino_Nano:Socket_Strip_Arduino_1x15" H 4700 3300 50  0001 C CNN
F 3 "" H 4700 3300 50  0000 C CNN
	1    4700 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 56D7422C
P 3800 1250
F 0 "#PWR01" H 3800 1000 50  0001 C CNN
F 1 "GND" H 3800 1100 50  0000 C CNN
F 2 "" H 3800 1250 50  0000 C CNN
F 3 "" H 3800 1250 50  0000 C CNN
	1    3800 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 1150 3850 1150
Wire Wire Line
	3850 1350 4500 1350
Wire Wire Line
	4500 1450 3850 1450
Wire Wire Line
	3850 1550 4500 1550
Wire Wire Line
	4500 1650 3850 1650
Wire Wire Line
	3850 1750 4500 1750
Wire Wire Line
	4500 1850 3850 1850
Wire Wire Line
	3850 1950 4500 1950
Wire Wire Line
	4500 2050 3850 2050
$Comp
L power:GND #PWR02
U 1 1 56D746ED
P 3550 2700
F 0 "#PWR02" H 3550 2450 50  0001 C CNN
F 1 "GND" H 3550 2550 50  0000 C CNN
F 2 "" H 3550 2700 50  0000 C CNN
F 3 "" H 3550 2700 50  0000 C CNN
	1    3550 2700
	0    1    -1   0   
$EndComp
Wire Wire Line
	4500 2600 4400 2600
$Comp
L power:+5V #PWR03
U 1 1 56D747E8
P 3600 2900
F 0 "#PWR03" H 3600 2750 50  0001 C CNN
F 1 "+5V" V 3600 3100 28  0000 C CNN
F 2 "" H 3600 2900 50  0000 C CNN
F 3 "" H 3600 2900 50  0000 C CNN
	1    3600 2900
	0    -1   1    0   
$EndComp
Wire Wire Line
	4100 2800 4500 2800
Wire Wire Line
	4500 3000 4100 3000
Wire Wire Line
	4100 3100 4500 3100
Wire Wire Line
	4100 3200 4500 3200
Wire Wire Line
	4500 3300 4100 3300
Wire Wire Line
	4100 3400 4500 3400
Wire Wire Line
	4100 3500 4500 3500
Wire Wire Line
	4500 3600 4100 3600
Wire Wire Line
	4100 3700 4500 3700
Wire Wire Line
	4100 3800 4500 3800
Text Notes 4800 950  0    60   ~ 0
1
Wire Wire Line
	4500 2900 3600 2900
Wire Wire Line
	4500 3900 3600 3900
Text Notes 3150 725  0    60   ~ 0
Shield for Arduino Nano
Wire Wire Line
	4500 2700 3550 2700
Wire Wire Line
	3800 1250 4500 1250
Text GLabel 4500 2350 0    50   Input ~ 0
MISO
Text GLabel 4500 2250 0    50   Input ~ 0
MOSI
Text GLabel 4500 4000 0    50   Input ~ 0
SCK
$Comp
L power:+3V3 #PWR04
U 1 1 56D74854
P 3600 3900
F 0 "#PWR04" H 3600 3750 50  0001 C CNN
F 1 "+3.3V" V 3600 4100 28  0000 C CNN
F 2 "" H 3600 3900 50  0000 C CNN
F 3 "" H 3600 3900 50  0000 C CNN
	1    3600 3900
	0    -1   1    0   
$EndComp
$Sheet
S 3100 5900 2000 450 
U 60193DF4
F0 "SensorsAndCommunication" 50
F1 "SensorsAndCommunication.sch" 50
$EndSheet
$Sheet
S 3100 4400 2000 500 
U 602705E1
F0 "Motor" 50
F1 "Motor.sch" 50
$EndSheet
Text GLabel 4500 2150 0    50   Input ~ 0
CS_RADIO
Text GLabel 4500 950  0    50   Input ~ 0
TX
Text GLabel 4500 1050 0    50   Input ~ 0
RX
Wire Notes Line
	4300 600  4300 750 
Wire Notes Line
	4300 750  3100 750 
Wire Notes Line
	3100 600  5100 600 
$Sheet
S 3100 5150 2000 500 
U 601C2277
F0 "PowerSupply" 50
F1 "PowerSupply.sch" 50
$EndSheet
Wire Notes Line
	5100 4100 3100 4100
Wire Notes Line
	3100 600  3100 4100
Wire Notes Line
	5100 600  5100 4100
Wire Notes Line
	3000 600  3000 6450
Wire Notes Line
	3000 6450 700  6450
Wire Notes Line
	700  6450 700  600 
Wire Notes Line
	700  600  3000 600 
Text Notes 750  1800 0    50   ~ 0
Project Description\n\nDIY Hexacopter Project with a focus on:\n- using a super capacitor for energy storage\n- minimizing cost of drone\n- minimizing count of hand-soldered components\n\n\nFlight controller - Arduino\nCommunication - ESP32\nFrame - PCB\nMotor - Easton clone\nProp - Easton clone\n
$EndSCHEMATC
