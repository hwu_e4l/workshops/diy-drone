EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF:NRF24L01 U?
U 1 1 5F7014BC
P 2250 2450
AR Path="/60193DF4/5F7014BC" Ref="U?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F7014BC" Ref="U2"  Part="1" 
F 0 "U2" H 2450 3250 50  0000 C CNN
F 1 "NRF24L01" H 2600 3150 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-20-1EP_4x4mm_P0.5mm_EP2.5x2.5mm" H 2450 3250 50  0001 L CIN
F 3 "http://www.nordicsemi.com/eng/content/download/2730/34105/file/nRF24L01_Product_Specification_v2_0.pdf" H 2250 2550 50  0001 C CNN
	1    2250 2450
	1    0    0    -1  
$EndComp
Text GLabel 1650 2050 0    50   Input ~ 0
MISO
Text GLabel 1650 1950 0    50   Input ~ 0
MOSI
Text GLabel 1650 2150 0    50   Input ~ 0
SCK
Text GLabel 1650 2250 0    50   Input ~ 0
CS_RADIO
Text GLabel 1650 2450 0    50   Input ~ 0
CE
Text GLabel 1650 2550 0    50   Input ~ 0
IRQ
Text Label 1650 2750 2    50   ~ 0
IREF
Text Label 1900 3700 2    50   ~ 0
IREF
Text Label 1900 4050 2    50   ~ 0
VDD
Text Label 2150 3250 3    50   ~ 0
VSS
Text Label 2250 3250 3    50   ~ 0
VSS
Text Label 2350 3250 3    50   ~ 0
VSS
Text Label 2450 3250 3    50   ~ 0
VSS
Text Label 1900 3900 2    50   ~ 0
VSS
$Comp
L power:GND #PWR?
U 1 1 5F7014D0
P 2100 3900
AR Path="/60193DF4/5F7014D0" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F7014D0" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 2100 3650 50  0001 C CNN
F 1 "GND" V 2105 3772 50  0000 R CNN
F 2 "" H 2100 3900 50  0001 C CNN
F 3 "" H 2100 3900 50  0001 C CNN
	1    2100 3900
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F7014D6
P 2050 4050
AR Path="/60193DF4/5F7014D6" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F7014D6" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 2050 3900 50  0001 C CNN
F 1 "+3.3V" V 2065 4178 50  0000 L CNN
F 2 "" H 2050 4050 50  0001 C CNN
F 3 "" H 2050 4050 50  0001 C CNN
	1    2050 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 3900 2100 3900
Wire Wire Line
	1900 4050 2050 4050
Text Label 2150 1650 1    50   ~ 0
VDD
Text Label 2250 1650 1    50   ~ 0
VDD
Text Label 2350 1650 1    50   ~ 0
VDD
$Comp
L Device:R_Small_US R?
U 1 1 5F7014E1
P 2100 3700
AR Path="/60193DF4/5F7014E1" Ref="R?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F7014E1" Ref="R1"  Part="1" 
F 0 "R1" V 1895 3700 50  0000 C CNN
F 1 "22k" V 1986 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2100 3700 50  0001 C CNN
F 3 "~" H 2100 3700 50  0001 C CNN
	1    2100 3700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7014E7
P 2300 3700
AR Path="/60193DF4/5F7014E7" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F7014E7" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 2300 3450 50  0001 C CNN
F 1 "GND" V 2305 3572 50  0000 R CNN
F 2 "" H 2300 3700 50  0001 C CNN
F 3 "" H 2300 3700 50  0001 C CNN
	1    2300 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1900 3700 2000 3700
Wire Wire Line
	2200 3700 2300 3700
Text Label 2850 2750 0    50   ~ 0
XC1
Text Label 2850 2950 0    50   ~ 0
XC2
Text Label 2850 2150 0    50   ~ 0
ANT1
Text Label 2850 2350 0    50   ~ 0
ANT2
Text Label 1650 2950 2    50   ~ 0
DVDD
Text Label 2850 1950 0    50   ~ 0
VDD_PA
$Comp
L Device:R_Small_US R2
U 1 1 5F702FD8
P 5700 2300
F 0 "R2" H 5632 2254 50  0000 R CNN
F 1 "10k" H 5632 2345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 2300 50  0001 C CNN
F 3 "~" H 5700 2300 50  0001 C CNN
	1    5700 2300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F704D7B
P 5700 2050
AR Path="/60193DF4/5F704D7B" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F704D7B" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 5700 1900 50  0001 C CNN
F 1 "+3.3V" H 5715 2223 50  0000 C CNN
F 2 "" H 5700 2050 50  0001 C CNN
F 3 "" H 5700 2050 50  0001 C CNN
	1    5700 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F705706
P 5700 3050
AR Path="/60193DF4/5F705706" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F705706" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 5700 2800 50  0001 C CNN
F 1 "GND" H 5705 2877 50  0000 C CNN
F 2 "" H 5700 3050 50  0001 C CNN
F 3 "" H 5700 3050 50  0001 C CNN
	1    5700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2050 5700 2200
Text GLabel 6850 2550 2    50   Input ~ 0
MISO
Text GLabel 5500 2550 0    50   Input ~ 0
MOSI
Text GLabel 7350 2550 0    50   Input ~ 0
SCK
Text GLabel 8400 2550 0    50   Input ~ 0
CS_RADIO
$Comp
L Device:C_Small C16
U 1 1 5F70AC86
P 5700 2800
F 0 "C16" H 5792 2846 50  0000 L CNN
F 1 "50 pF" H 5792 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5700 2800 50  0001 C CNN
F 3 "~" H 5700 2800 50  0001 C CNN
	1    5700 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2900 5700 3050
Text GLabel 5900 2550 2    50   Input ~ 0
MOSI
Wire Wire Line
	5700 2400 5700 2550
Wire Wire Line
	5500 2550 5700 2550
Connection ~ 5700 2550
Wire Wire Line
	5700 2550 5700 2700
Wire Wire Line
	5900 2550 5700 2550
$Comp
L Device:R_Small_US R4
U 1 1 5F719A2F
P 6650 2300
F 0 "R4" H 6582 2254 50  0000 R CNN
F 1 "10k" H 6582 2345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6650 2300 50  0001 C CNN
F 3 "~" H 6650 2300 50  0001 C CNN
	1    6650 2300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F719A35
P 6650 2050
AR Path="/60193DF4/5F719A35" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F719A35" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 6650 1900 50  0001 C CNN
F 1 "+3.3V" H 6665 2223 50  0000 C CNN
F 2 "" H 6650 2050 50  0001 C CNN
F 3 "" H 6650 2050 50  0001 C CNN
	1    6650 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F719A3B
P 6650 3050
AR Path="/60193DF4/5F719A3B" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F719A3B" Ref="#PWR0131"  Part="1" 
F 0 "#PWR0131" H 6650 2800 50  0001 C CNN
F 1 "GND" H 6655 2877 50  0000 C CNN
F 2 "" H 6650 3050 50  0001 C CNN
F 3 "" H 6650 3050 50  0001 C CNN
	1    6650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2050 6650 2200
$Comp
L Device:C_Small C19
U 1 1 5F719A43
P 6650 2800
F 0 "C19" H 6742 2846 50  0000 L CNN
F 1 "50 pF" H 6742 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6650 2800 50  0001 C CNN
F 3 "~" H 6650 2800 50  0001 C CNN
	1    6650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2900 6650 3050
Wire Wire Line
	6650 2400 6650 2550
Wire Wire Line
	6450 2550 6650 2550
Connection ~ 6650 2550
Wire Wire Line
	6650 2550 6650 2700
Wire Wire Line
	6850 2550 6650 2550
$Comp
L Device:R_Small_US R5
U 1 1 5F71A6A8
P 7550 2300
F 0 "R5" H 7482 2254 50  0000 R CNN
F 1 "10k" H 7482 2345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7550 2300 50  0001 C CNN
F 3 "~" H 7550 2300 50  0001 C CNN
	1    7550 2300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F71A6AE
P 7550 2050
AR Path="/60193DF4/5F71A6AE" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F71A6AE" Ref="#PWR0132"  Part="1" 
F 0 "#PWR0132" H 7550 1900 50  0001 C CNN
F 1 "+3.3V" H 7565 2223 50  0000 C CNN
F 2 "" H 7550 2050 50  0001 C CNN
F 3 "" H 7550 2050 50  0001 C CNN
	1    7550 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F71A6B4
P 7550 3050
AR Path="/60193DF4/5F71A6B4" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F71A6B4" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 7550 2800 50  0001 C CNN
F 1 "GND" H 7555 2877 50  0000 C CNN
F 2 "" H 7550 3050 50  0001 C CNN
F 3 "" H 7550 3050 50  0001 C CNN
	1    7550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2050 7550 2200
$Comp
L Device:C_Small C20
U 1 1 5F71A6BC
P 7550 2800
F 0 "C20" H 7642 2846 50  0000 L CNN
F 1 "50 pF" H 7642 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7550 2800 50  0001 C CNN
F 3 "~" H 7550 2800 50  0001 C CNN
	1    7550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2900 7550 3050
Wire Wire Line
	7550 2400 7550 2550
Wire Wire Line
	7350 2550 7550 2550
Connection ~ 7550 2550
Wire Wire Line
	7550 2550 7550 2700
Wire Wire Line
	7750 2550 7550 2550
$Comp
L Device:R_Small_US R6
U 1 1 5F71AD9C
P 8600 2300
F 0 "R6" H 8532 2254 50  0000 R CNN
F 1 "10k" H 8532 2345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8600 2300 50  0001 C CNN
F 3 "~" H 8600 2300 50  0001 C CNN
	1    8600 2300
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F71ADA2
P 8600 2050
AR Path="/60193DF4/5F71ADA2" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F71ADA2" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 8600 1900 50  0001 C CNN
F 1 "+3.3V" H 8615 2223 50  0000 C CNN
F 2 "" H 8600 2050 50  0001 C CNN
F 3 "" H 8600 2050 50  0001 C CNN
	1    8600 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F71ADA8
P 8600 3050
AR Path="/60193DF4/5F71ADA8" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F71ADA8" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 8600 2800 50  0001 C CNN
F 1 "GND" H 8605 2877 50  0000 C CNN
F 2 "" H 8600 3050 50  0001 C CNN
F 3 "" H 8600 3050 50  0001 C CNN
	1    8600 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2050 8600 2200
$Comp
L Device:C_Small C21
U 1 1 5F71ADB0
P 8600 2800
F 0 "C21" H 8692 2846 50  0000 L CNN
F 1 "50 pF" H 8692 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8600 2800 50  0001 C CNN
F 3 "~" H 8600 2800 50  0001 C CNN
	1    8600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2900 8600 3050
Wire Wire Line
	8600 2400 8600 2550
Wire Wire Line
	8400 2550 8600 2550
Connection ~ 8600 2550
Wire Wire Line
	8600 2550 8600 2700
Wire Wire Line
	8800 2550 8600 2550
Text GLabel 6450 2550 0    50   Input ~ 0
MISO
Text GLabel 7750 2550 2    50   Input ~ 0
SCK
Text GLabel 8800 2550 2    50   Input ~ 0
CS_RADIO
Text Notes 5050 1600 0    50   ~ 0
External Pullup for SPI pins
Wire Notes Line
	5000 1500 5000 3500
Wire Notes Line
	5000 3500 9500 3500
Wire Notes Line
	9500 3500 9500 1500
Wire Notes Line
	9500 1500 5000 1500
Wire Notes Line
	5000 1650 6200 1650
Wire Notes Line
	6200 1650 6200 1500
$Comp
L dk_Crystals:NX3225GD-8MHZ-STD-CRA-3 XTAL1
U 1 1 5F74B7A0
P 6200 4800
F 0 "XTAL1" V 6154 4903 50  0000 L CNN
F 1 "NX3225GD-8MHZ-STD-CRA-3" V 6245 4903 50  0000 L CNN
F 2 "digikey-footprints:SMD-2_3.2x2.5mm" H 6400 5000 60  0001 L CNN
F 3 "http://www.ndk.com/images/products/catalog/c_NX3225GD-STD-CRA-3_e.pdf" H 6400 5100 60  0001 L CNN
F 4 "644-1178-1-ND" H 6400 5200 60  0001 L CNN "Digi-Key_PN"
F 5 "NX3225GD-8MHZ-STD-CRA-3" H 6400 5300 60  0001 L CNN "MPN"
F 6 "Crystals, Oscillators, Resonators" H 6400 5400 60  0001 L CNN "Category"
F 7 "Crystals" H 6400 5500 60  0001 L CNN "Family"
F 8 "http://www.ndk.com/images/products/catalog/c_NX3225GD-STD-CRA-3_e.pdf" H 6400 5600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ndk-america-inc/NX3225GD-8MHZ-STD-CRA-3/644-1178-1-ND/3125567" H 6400 5700 60  0001 L CNN "DK_Detail_Page"
F 10 "CRYSTAL 8.0000MHZ 8PF SMD" H 6400 5800 60  0001 L CNN "Description"
F 11 "NDK America, Inc." H 6400 5900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6400 6000 60  0001 L CNN "Status"
	1    6200 4800
	0    1    1    0   
$EndComp
Text Label 5300 4600 2    50   ~ 0
XC1
Text Label 5300 5000 2    50   ~ 0
XC2
$Comp
L Device:R_Small_US R3
U 1 1 5F74EABA
P 5850 4800
F 0 "R3" H 5918 4846 50  0000 L CNN
F 1 "1M" H 5918 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5850 4800 50  0001 C CNN
F 3 "~" H 5850 4800 50  0001 C CNN
	1    5850 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4600 5850 4600
Wire Wire Line
	6200 5000 5850 5000
Connection ~ 5850 4600
Wire Wire Line
	5850 4600 6200 4600
Connection ~ 5850 5000
Wire Wire Line
	5850 5000 5300 5000
Wire Wire Line
	6200 4600 6200 4650
Wire Wire Line
	6200 4950 6200 5000
Wire Wire Line
	5850 4900 5850 5000
Wire Wire Line
	5850 4600 5850 4700
$Comp
L Device:C_Small C18
U 1 1 5F7563F2
P 6050 5300
F 0 "C18" V 6279 5300 50  0000 C CNN
F 1 "C_Small" V 6188 5300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6050 5300 50  0001 C CNN
F 3 "~" H 6050 5300 50  0001 C CNN
	1    6050 5300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F75AE98
P 6350 5300
AR Path="/60193DF4/5F75AE98" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F75AE98" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 6350 5050 50  0001 C CNN
F 1 "GND" V 6355 5172 50  0000 R CNN
F 2 "" H 6350 5300 50  0001 C CNN
F 3 "" H 6350 5300 50  0001 C CNN
	1    6350 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 5300 6150 5300
Wire Wire Line
	5950 5300 5850 5300
Wire Wire Line
	5850 5300 5850 5000
$Comp
L Device:C_Small C17
U 1 1 5F760D5C
P 6050 4300
F 0 "C17" V 6279 4300 50  0000 C CNN
F 1 "C_Small" V 6188 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6050 4300 50  0001 C CNN
F 3 "~" H 6050 4300 50  0001 C CNN
	1    6050 4300
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F760D62
P 6350 4300
AR Path="/60193DF4/5F760D62" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F6F1722/5F760D62" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 6350 4050 50  0001 C CNN
F 1 "GND" V 6355 4172 50  0000 R CNN
F 2 "" H 6350 4300 50  0001 C CNN
F 3 "" H 6350 4300 50  0001 C CNN
	1    6350 4300
	0    -1   1    0   
$EndComp
Wire Wire Line
	6350 4300 6150 4300
Wire Wire Line
	5950 4300 5850 4300
Wire Wire Line
	5850 4300 5850 4600
Text Notes 5050 3900 0    50   ~ 0
External Pullup for SPI pins
Wire Notes Line
	5000 3800 5000 5800
Wire Notes Line
	9500 3800 5000 3800
Wire Notes Line
	5000 3950 6200 3950
$EndSCHEMATC
