EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5827 8268
encoding utf-8
Sheet 7 14
Title "Hexacopter Motor Control"
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3650 4900 500  250 
U 60274500
F0 "Motor_A" 50
F1 "Motor_A.sch" 50
$EndSheet
$Sheet
S 4350 4900 500  250 
U 60274502
F0 "Motor_B" 50
F1 "Motor_B.sch" 50
$EndSheet
$Sheet
S 4500 5350 500  250 
U 60274504
F0 "Motor_C" 50
F1 "Motor_C.sch" 50
$EndSheet
$Sheet
S 4350 5800 500  250 
U 602A886A
F0 "Motor_D" 50
F1 "Motor_D.sch" 50
$EndSheet
$Comp
L power:+3.3V #PWR0147
U 1 1 603186F2
P 3400 1700
F 0 "#PWR0147" H 3400 1550 50  0001 C CNN
F 1 "+3.3V" H 3415 1873 50  0000 C CNN
F 2 "" H 3400 1700 50  0001 C CNN
F 3 "" H 3400 1700 50  0001 C CNN
	1    3400 1700
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP2
U 1 1 60318AA2
P 3600 1850
F 0 "JP2" H 3600 2055 50  0000 C CNN
F 1 "SJ_A0" H 3600 1964 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 1850 50  0001 C CNN
F 3 "~" H 3600 1850 50  0001 C CNN
	1    3600 1850
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP3
U 1 1 603196FF
P 3600 2200
F 0 "JP3" H 3600 2405 50  0000 C CNN
F 1 "SJ_A1" H 3600 2314 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 2200 50  0001 C CNN
F 3 "~" H 3600 2200 50  0001 C CNN
	1    3600 2200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP4
U 1 1 60319C4B
P 3600 2550
F 0 "JP4" H 3600 2755 50  0000 C CNN
F 1 "SJ_A2" H 3600 2664 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 2550 50  0001 C CNN
F 3 "~" H 3600 2550 50  0001 C CNN
	1    3600 2550
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP5
U 1 1 60319C51
P 3600 2900
F 0 "JP5" H 3600 3105 50  0000 C CNN
F 1 "SJ_A3" H 3600 3014 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 2900 50  0001 C CNN
F 3 "~" H 3600 2900 50  0001 C CNN
	1    3600 2900
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP6
U 1 1 6031A49B
P 3600 3250
F 0 "JP6" H 3600 3455 50  0000 C CNN
F 1 "SJ_A4" H 3600 3364 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 3250 50  0001 C CNN
F 3 "~" H 3600 3250 50  0001 C CNN
	1    3600 3250
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP7
U 1 1 6031A4A1
P 3600 3600
F 0 "JP7" H 3600 3805 50  0000 C CNN
F 1 "SJ_A5" H 3600 3714 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3600 3600 50  0001 C CNN
F 3 "~" H 3600 3600 50  0001 C CNN
	1    3600 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R12
U 1 1 6031CFC7
P 3950 3850
F 0 "R12" H 3882 3804 50  0000 R CNN
F 1 "10k" H 3882 3895 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3950 3850 50  0001 C CNN
F 3 "~" H 3950 3850 50  0001 C CNN
	1    3950 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3600 3950 3600
$Comp
L Device:R_Small_US R13
U 1 1 6032136A
P 4150 3500
F 0 "R13" H 4082 3454 50  0000 R CNN
F 1 "10k" H 4082 3545 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4150 3500 50  0001 C CNN
F 3 "~" H 4150 3500 50  0001 C CNN
	1    4150 3500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R14
U 1 1 60321B38
P 4350 3150
F 0 "R14" H 4282 3104 50  0000 R CNN
F 1 "10k" H 4282 3195 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4350 3150 50  0001 C CNN
F 3 "~" H 4350 3150 50  0001 C CNN
	1    4350 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R15
U 1 1 60321B3E
P 4550 2800
F 0 "R15" H 4482 2754 50  0000 R CNN
F 1 "10k" H 4482 2845 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4550 2800 50  0001 C CNN
F 3 "~" H 4550 2800 50  0001 C CNN
	1    4550 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R16
U 1 1 603224EA
P 4750 2450
F 0 "R16" H 4682 2404 50  0000 R CNN
F 1 "10k" H 4682 2495 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4750 2450 50  0001 C CNN
F 3 "~" H 4750 2450 50  0001 C CNN
	1    4750 2450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R17
U 1 1 603224F0
P 4950 2100
F 0 "R17" H 4882 2054 50  0000 R CNN
F 1 "10k" H 4882 2145 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4950 2100 50  0001 C CNN
F 3 "~" H 4950 2100 50  0001 C CNN
	1    4950 2100
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 6032C172
P 4950 1650
F 0 "TP6" V 4904 1838 50  0000 L CNN
F 1 "TestPoint" V 4995 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 5150 1650 50  0001 C CNN
F 3 "~" H 5150 1650 50  0001 C CNN
	1    4950 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 6032CE1B
P 4750 1650
F 0 "TP5" V 4704 1838 50  0000 L CNN
F 1 "TestPoint" V 4795 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 4950 1650 50  0001 C CNN
F 3 "~" H 4950 1650 50  0001 C CNN
	1    4750 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 6032D1E2
P 4550 1650
F 0 "TP4" V 4504 1838 50  0000 L CNN
F 1 "TestPoint" V 4595 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 4750 1650 50  0001 C CNN
F 3 "~" H 4750 1650 50  0001 C CNN
	1    4550 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 6032D4D3
P 4350 1650
F 0 "TP3" V 4304 1838 50  0000 L CNN
F 1 "TestPoint" V 4395 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 4550 1650 50  0001 C CNN
F 3 "~" H 4550 1650 50  0001 C CNN
	1    4350 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 6032D765
P 4150 1650
F 0 "TP2" V 4104 1838 50  0000 L CNN
F 1 "TestPoint" V 4195 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 4350 1650 50  0001 C CNN
F 3 "~" H 4350 1650 50  0001 C CNN
	1    4150 1650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 6032D9FC
P 3950 1650
F 0 "TP1" V 3904 1838 50  0000 L CNN
F 1 "TestPoint" V 3995 1838 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 4150 1650 50  0001 C CNN
F 3 "~" H 4150 1650 50  0001 C CNN
	1    3950 1650
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 6032FB89
P 5050 4050
F 0 "#PWR0148" H 5050 3800 50  0001 C CNN
F 1 "GND" H 5055 3877 50  0000 C CNN
F 2 "" H 5050 4050 50  0001 C CNN
F 3 "" H 5050 4050 50  0001 C CNN
	1    5050 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3950 3950 4000
Text Label 3800 1850 0    50   ~ 0
A0
Text Label 3800 2200 0    50   ~ 0
A1
Text Label 3800 2550 0    50   ~ 0
A2
Text Label 3800 2900 0    50   ~ 0
A3
Text Label 3800 3250 0    50   ~ 0
A4
Text Label 3800 3600 0    50   ~ 0
A5
$Sheet
S 3650 5800 500  250 
U 602A8868
F0 "Motor_E" 50
F1 "Motor_E.sch" 50
$EndSheet
$Sheet
S 3500 5350 500  250 
U 60274506
F0 "Motor_F" 50
F1 "Motor_F.sch" 50
$EndSheet
Wire Wire Line
	2500 6050 2500 6200
Connection ~ 2500 6050
Wire Wire Line
	2350 6050 2500 6050
Wire Wire Line
	2800 6050 2800 6200
Connection ~ 2800 6050
Wire Wire Line
	2950 6050 2800 6050
Wire Wire Line
	2500 5900 2500 6050
Wire Wire Line
	2800 5900 2800 6050
$Comp
L power:GND #PWR0149
U 1 1 6034993E
P 2800 6200
F 0 "#PWR0149" H 2800 5950 50  0001 C CNN
F 1 "GND" H 2805 6027 50  0000 C CNN
F 2 "" H 2800 6200 50  0001 C CNN
F 3 "" H 2800 6200 50  0001 C CNN
	1    2800 6200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 60347DA8
P 2500 6200
F 0 "#PWR0150" H 2500 5950 50  0001 C CNN
F 1 "GND" H 2505 6027 50  0000 C CNN
F 2 "" H 2500 6200 50  0001 C CNN
F 3 "" H 2500 6200 50  0001 C CNN
	1    2500 6200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0151
U 1 1 60346D25
P 2500 5900
F 0 "#PWR0151" H 2500 5750 50  0001 C CNN
F 1 "+3.3V" H 2515 6073 50  0000 C CNN
F 2 "" H 2500 5900 50  0001 C CNN
F 3 "" H 2500 5900 50  0001 C CNN
	1    2500 5900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0152
U 1 1 603463D3
P 2800 5900
F 0 "#PWR0152" H 2800 5750 50  0001 C CNN
F 1 "+3.3V" H 2815 6073 50  0000 C CNN
F 2 "" H 2800 5900 50  0001 C CNN
F 3 "" H 2800 5900 50  0001 C CNN
	1    2800 5900
	-1   0    0    -1  
$EndComp
Text GLabel 2350 6050 0    50   Input ~ 0
SDA
Text GLabel 2950 6050 2    50   Input ~ 0
SCL
Wire Wire Line
	3950 3600 3950 1650
Wire Wire Line
	4350 2900 4350 1650
Wire Wire Line
	4550 2550 4550 1650
Wire Wire Line
	4750 2200 4750 1650
Wire Wire Line
	4950 1850 4950 1650
Wire Notes Line
	3250 4450 5250 4450
Wire Notes Line
	5250 4450 5250 6450
Wire Notes Line
	5250 6450 3250 6450
Wire Notes Line
	3250 6450 3250 4450
Text Notes 3300 4550 0    50   ~ 0
Motor Schematics
Wire Notes Line
	3250 4600 4000 4600
Wire Notes Line
	4000 4600 4000 4450
Wire Notes Line
	3250 4350 3250 850 
Text Notes 3300 950  0    50   ~ 0
I2C Address Selection for PWM Generator
Wire Notes Line
	4950 1000 4950 850 
Wire Notes Line
	3250 1000 4950 1000
Wire Wire Line
	3400 1700 3400 1850
Wire Wire Line
	3450 1850 3400 1850
Connection ~ 3400 1850
Wire Wire Line
	3400 1850 3400 2200
Wire Wire Line
	3450 2200 3400 2200
Connection ~ 3400 2200
Wire Wire Line
	3400 2200 3400 2550
Wire Wire Line
	3450 2550 3400 2550
Connection ~ 3400 2550
Wire Wire Line
	3400 2550 3400 2900
Wire Wire Line
	3450 2900 3400 2900
Connection ~ 3400 2900
Wire Wire Line
	3400 2900 3400 3250
Wire Wire Line
	3450 3600 3400 3600
Wire Wire Line
	3450 3250 3400 3250
Connection ~ 3400 3250
Wire Wire Line
	3400 3250 3400 3600
Wire Wire Line
	3950 3750 3950 3600
Connection ~ 3950 3600
Wire Wire Line
	4150 1650 4150 3250
Wire Wire Line
	3750 3250 4150 3250
Connection ~ 4150 3250
Wire Wire Line
	4150 3250 4150 3400
Wire Wire Line
	3750 2900 4350 2900
Wire Wire Line
	4350 2900 4350 3050
Connection ~ 4350 2900
Wire Wire Line
	3750 2550 4550 2550
Wire Wire Line
	4550 2550 4550 2700
Connection ~ 4550 2550
Wire Wire Line
	3750 2200 4750 2200
Wire Wire Line
	4750 2200 4750 2350
Connection ~ 4750 2200
Wire Wire Line
	3750 1850 4950 1850
Wire Wire Line
	4950 1850 4950 2000
Connection ~ 4950 1850
Wire Wire Line
	4150 3600 4150 3650
Wire Wire Line
	4350 3250 4350 3300
Wire Wire Line
	4550 2900 4550 2950
Wire Wire Line
	4750 2550 4750 2600
Wire Wire Line
	4950 2200 4950 2250
Wire Wire Line
	4950 2250 5050 2250
Wire Wire Line
	5050 2250 5050 2600
Wire Wire Line
	3950 4000 5050 4000
Connection ~ 5050 4000
Wire Wire Line
	5050 4000 5050 4050
Wire Wire Line
	4150 3650 5050 3650
Connection ~ 5050 3650
Wire Wire Line
	5050 3650 5050 4000
Wire Wire Line
	4350 3300 5050 3300
Connection ~ 5050 3300
Wire Wire Line
	5050 3300 5050 3650
Wire Wire Line
	4550 2950 5050 2950
Connection ~ 5050 2950
Wire Wire Line
	5050 2950 5050 3300
Wire Wire Line
	4750 2600 5050 2600
Connection ~ 5050 2600
Wire Wire Line
	5050 2600 5050 2950
Wire Notes Line
	3250 850  5250 850 
Wire Notes Line
	5250 4350 3250 4350
Wire Notes Line
	5250 850  5250 4350
Wire Wire Line
	1600 6100 1600 6200
Wire Wire Line
	850  4600 850  4850
Connection ~ 850  4600
Wire Wire Line
	900  4600 850  4600
Wire Wire Line
	850  4500 900  4500
$Comp
L Driver_LED:PCA9685PW U4
U 1 1 60313C7E
P 1600 5000
F 0 "U4" H 1700 5950 50  0000 C CNN
F 1 "PCA9685PW" H 1850 5850 50  0000 C CNN
F 2 "Package_SO:TSSOP-28_4.4x9.7mm_P0.65mm" H 1625 4025 50  0001 L CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCA9685.pdf" H 1200 5700 50  0001 C CNN
	1    1600 5000
	1    0    0    -1  
$EndComp
Text GLabel 900  4300 0    50   Input ~ 0
SCL
Text GLabel 900  4400 0    50   Input ~ 0
SDA
$Comp
L power:GND #PWR0153
U 1 1 60316D3D
P 1600 6200
F 0 "#PWR0153" H 1600 5950 50  0001 C CNN
F 1 "GND" H 1605 6027 50  0000 C CNN
F 2 "" H 1600 6200 50  0001 C CNN
F 3 "" H 1600 6200 50  0001 C CNN
	1    1600 6200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0154
U 1 1 6031704C
P 1600 4000
F 0 "#PWR0154" H 1600 3850 50  0001 C CNN
F 1 "+3.3V" H 1615 4173 50  0000 C CNN
F 2 "" H 1600 4000 50  0001 C CNN
F 3 "" H 1600 4000 50  0001 C CNN
	1    1600 4000
	1    0    0    -1  
$EndComp
Text Label 900  5200 2    50   ~ 0
A0
Text Label 900  5300 2    50   ~ 0
A1
Text Label 900  5400 2    50   ~ 0
A2
Text Label 900  5500 2    50   ~ 0
A3
Text Label 900  5600 2    50   ~ 0
A4
Text Label 900  5700 2    50   ~ 0
A5
$Comp
L power:GND #PWR0155
U 1 1 603431BF
P 850 4850
F 0 "#PWR0155" H 850 4600 50  0001 C CNN
F 1 "GND" H 855 4677 50  0000 C CNN
F 2 "" H 850 4850 50  0001 C CNN
F 3 "" H 850 4850 50  0001 C CNN
	1    850  4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  4500 850  4600
Text GLabel 2300 4500 2    50   Input ~ 0
HBRIDGE_B_HIGH
Text GLabel 2300 4600 2    50   Input ~ 0
HBRIDGE_B_LOW
Text GLabel 2300 4300 2    50   Input ~ 0
HBRIDGE_A_HIGH
Text GLabel 2300 4400 2    50   Input ~ 0
HBRIDGE_A_LOW
Text GLabel 2300 4700 2    50   Input ~ 0
HBRIDGE_C_HIGH
Text GLabel 2300 4800 2    50   Input ~ 0
HBRIDGE_C_LOW
Text GLabel 2300 5100 2    50   Input ~ 0
HBRIDGE_E_HIGH
Text GLabel 2300 5200 2    50   Input ~ 0
HBRIDGE_E_LOW
Text GLabel 2300 4900 2    50   Input ~ 0
HBRIDGE_D_HIGH
Text GLabel 2300 5000 2    50   Input ~ 0
HBRIDGE_D_LOW
Text GLabel 2300 5300 2    50   Input ~ 0
HBRIDGE_F_HIGH
Text GLabel 2300 5400 2    50   Input ~ 0
HBRIDGE_F_LOW
NoConn ~ 2350 5500
NoConn ~ 2350 5600
NoConn ~ 2350 5700
NoConn ~ 2350 5800
Wire Notes Line
	650  6450 3150 6450
Wire Notes Line
	3150 3450 650  3450
Text Notes 700  3550 0    50   ~ 0
PWM Generator for DC Motor Control
Wire Notes Line
	2150 3600 2150 3450
Wire Notes Line
	650  3600 2150 3600
Wire Notes Line
	650  3450 650  6450
Wire Notes Line
	3150 6450 3150 3450
Wire Notes Line
	3150 850  650  850 
Text Notes 700  950  0    50   ~ 0
PWM Generator for DC Motor Control
Wire Notes Line
	650  1000 2150 1000
Wire Notes Line
	650  850  650  3350
Wire Notes Line
	650  3350 3150 3350
Wire Notes Line
	3150 3350 3150 850 
Wire Notes Line
	2150 1000 2150 850 
$EndSCHEMATC
