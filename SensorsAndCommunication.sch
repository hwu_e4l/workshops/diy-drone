EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5827 8268
encoding utf-8
Sheet 2 14
Title "Sensors and Communication"
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1050 2650 0    50   Input ~ 0
MISO
Text GLabel 1050 2550 0    50   Input ~ 0
MOSI
Text GLabel 1050 2750 0    50   Input ~ 0
SCK
Text GLabel 1050 2850 0    50   Input ~ 0
CSN
NoConn ~ 1050 2850
NoConn ~ 1050 2750
NoConn ~ 1050 2650
NoConn ~ 1050 2550
Text Notes 750  2400 0    50   ~ 0
Data and Control Interface\n\nThe nRF24L01+ has six 5V tolerant digital signals.\nStandard SPI with a maximum data rate of 10 Mbps
$Sheet
S 3350 1100 1500 500 
U 6035AB22
F0 "IMU" 50
F1 "IMU.sch" 50
$EndSheet
$Sheet
S 3350 2600 1500 500 
U 6035E6AC
F0 "ESP32" 50
F1 "ESP32.sch" 50
$EndSheet
$Sheet
S 3350 3350 1500 500 
U 5F6F1722
F0 "Radio" 50
F1 "Radio.sch" 50
$EndSheet
$Sheet
S 3350 1850 1500 500 
U 5F83AC9F
F0 "USB_Serial" 50
F1 "USB_Serial.sch" 50
$EndSheet
Text Label 7000 3150 2    50   ~ 0
VDD_SDIO
$Comp
L power:+3V3 #PWR?
U 1 1 5F851EB0
P 7200 3150
AR Path="/5F851EB0" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EB0" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 7200 3000 50  0001 C CNN
F 1 "+3.3V" V 7200 3350 28  0000 C CNN
F 2 "" H 7200 3150 50  0000 C CNN
F 3 "" H 7200 3150 50  0000 C CNN
	1    7200 3150
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851EB6
P 6750 2950
AR Path="/5F851EB6" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EB6" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 6750 2700 50  0001 C CNN
F 1 "GND" H 6750 2800 50  0000 C CNN
F 2 "" H 6750 2950 50  0000 C CNN
F 3 "" H 6750 2950 50  0000 C CNN
	1    6750 2950
	0    1    -1   0   
$EndComp
Wire Wire Line
	7200 3150 7100 3150
Wire Wire Line
	7100 3150 7100 2950
Wire Wire Line
	7100 3150 7000 3150
Connection ~ 7100 3150
$Comp
L power:+3V3 #PWR?
U 1 1 5F851EC0
P 7200 3700
AR Path="/5F851EC0" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EC0" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 7200 3550 50  0001 C CNN
F 1 "+3.3V" V 7200 3900 28  0000 C CNN
F 2 "" H 7200 3700 50  0000 C CNN
F 3 "" H 7200 3700 50  0000 C CNN
	1    7200 3700
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851EC6
P 6750 3500
AR Path="/5F851EC6" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EC6" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 6750 3250 50  0001 C CNN
F 1 "GND" H 6750 3350 50  0000 C CNN
F 2 "" H 6750 3500 50  0000 C CNN
F 3 "" H 6750 3500 50  0000 C CNN
	1    6750 3500
	0    1    -1   0   
$EndComp
Wire Wire Line
	7200 3700 7100 3700
Wire Wire Line
	7100 3700 7100 3500
Wire Wire Line
	7100 3700 7000 3700
Connection ~ 7100 3700
Text Label 7000 4300 2    50   ~ 0
VDD3P3_CPU
$Comp
L power:+3V3 #PWR?
U 1 1 5F851ED1
P 7200 4300
AR Path="/5F851ED1" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851ED1" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 7200 4150 50  0001 C CNN
F 1 "+3.3V" V 7200 4500 28  0000 C CNN
F 2 "" H 7200 4300 50  0000 C CNN
F 3 "" H 7200 4300 50  0000 C CNN
	1    7200 4300
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851ED7
P 6750 4100
AR Path="/5F851ED7" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851ED7" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 6750 3850 50  0001 C CNN
F 1 "GND" H 6750 3950 50  0000 C CNN
F 2 "" H 6750 4100 50  0000 C CNN
F 3 "" H 6750 4100 50  0000 C CNN
	1    6750 4100
	0    1    -1   0   
$EndComp
Wire Wire Line
	7200 4300 7100 4300
Wire Wire Line
	7100 4300 7100 4100
Wire Wire Line
	7100 4300 7000 4300
Connection ~ 7100 4300
$Comp
L Device:C C?
U 1 1 5F851EE1
P 6900 2950
AR Path="/5F851EE1" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851EE1" Ref="C1"  Part="1" 
F 0 "C1" V 7152 2950 50  0000 C CNN
F 1 "1 uF" V 7061 2950 50  0000 C CNN
F 2 "" H 6938 2800 50  0001 C CNN
F 3 "~" H 6900 2950 50  0001 C CNN
	1    6900 2950
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851EE7
P 6900 3500
AR Path="/5F851EE7" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851EE7" Ref="C2"  Part="1" 
F 0 "C2" V 7152 3500 50  0000 C CNN
F 1 "0.1 uF" V 7061 3500 50  0000 C CNN
F 2 "" H 6938 3350 50  0001 C CNN
F 3 "~" H 6900 3500 50  0001 C CNN
	1    6900 3500
	0    1    -1   0   
$EndComp
Text Label 7000 3700 2    50   ~ 0
VDD3P3_RTC
$Comp
L Device:C C?
U 1 1 5F851EEE
P 6900 4100
AR Path="/5F851EEE" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851EEE" Ref="C3"  Part="1" 
F 0 "C3" V 7152 4100 50  0000 C CNN
F 1 "0.1 uF" V 7061 4100 50  0000 C CNN
F 2 "" H 6938 3950 50  0001 C CNN
F 3 "~" H 6900 4100 50  0001 C CNN
	1    6900 4100
	0    1    -1   0   
$EndComp
Text Notes 6550 2550 0    50   ~ 0
Digital Power Supply
Text Label 8050 4300 2    50   ~ 0
VDDA
$Comp
L power:+3V3 #PWR?
U 1 1 5F851EF6
P 8250 4300
AR Path="/5F851EF6" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EF6" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 8250 4150 50  0001 C CNN
F 1 "+3.3V" V 8250 4500 28  0000 C CNN
F 2 "" H 8250 4300 50  0000 C CNN
F 3 "" H 8250 4300 50  0000 C CNN
	1    8250 4300
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851EFC
P 7800 4150
AR Path="/5F851EFC" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851EFC" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 7800 3900 50  0001 C CNN
F 1 "GND" H 7800 4000 50  0000 C CNN
F 2 "" H 7800 4150 50  0000 C CNN
F 3 "" H 7800 4150 50  0000 C CNN
	1    7800 4150
	0    1    -1   0   
$EndComp
Wire Wire Line
	8250 4300 8150 4300
Wire Wire Line
	8150 4300 8150 4100
Wire Wire Line
	8150 4300 8050 4300
Connection ~ 8150 4300
$Comp
L Device:C C?
U 1 1 5F851F06
P 7950 4100
AR Path="/5F851F06" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F06" Ref="C6"  Part="1" 
F 0 "C6" V 8202 4100 50  0000 C CNN
F 1 "0.1 uF" V 8111 4100 50  0000 C CNN
F 2 "" H 7988 3950 50  0001 C CNN
F 3 "~" H 7950 4100 50  0001 C CNN
	1    7950 4100
	0    1    -1   0   
$EndComp
Text Label 8050 3550 2    50   ~ 0
VDDA
$Comp
L power:+3V3 #PWR?
U 1 1 5F851F0D
P 8250 3550
AR Path="/5F851F0D" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F0D" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 8250 3400 50  0001 C CNN
F 1 "+3.3V" V 8250 3750 28  0000 C CNN
F 2 "" H 8250 3550 50  0000 C CNN
F 3 "" H 8250 3550 50  0000 C CNN
	1    8250 3550
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851F13
P 7800 3350
AR Path="/5F851F13" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F13" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 7800 3100 50  0001 C CNN
F 1 "GND" H 7800 3200 50  0000 C CNN
F 2 "" H 7800 3350 50  0000 C CNN
F 3 "" H 7800 3350 50  0000 C CNN
	1    7800 3350
	0    1    -1   0   
$EndComp
Wire Wire Line
	8250 3550 8150 3550
Wire Wire Line
	8150 3550 8050 3550
Connection ~ 8150 3550
$Comp
L Device:C C?
U 1 1 5F851F1C
P 7950 3350
AR Path="/5F851F1C" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F1C" Ref="C5"  Part="1" 
F 0 "C5" V 8202 3350 50  0000 C CNN
F 1 "1 uF" V 8111 3350 50  0000 C CNN
F 2 "" H 7988 3200 50  0001 C CNN
F 3 "~" H 7950 3350 50  0001 C CNN
	1    7950 3350
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851F22
P 7950 2950
AR Path="/5F851F22" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F22" Ref="C4"  Part="1" 
F 0 "C4" V 8202 2950 50  0000 C CNN
F 1 "100 pF" V 8111 2950 50  0000 C CNN
F 2 "" H 7988 2800 50  0001 C CNN
F 3 "~" H 7950 2950 50  0001 C CNN
	1    7950 2950
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851F28
P 7800 2950
AR Path="/5F851F28" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F28" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 7800 2700 50  0001 C CNN
F 1 "GND" H 7800 2800 50  0000 C CNN
F 2 "" H 7800 2950 50  0000 C CNN
F 3 "" H 7800 2950 50  0000 C CNN
	1    7800 2950
	0    1    -1   0   
$EndComp
Wire Wire Line
	8100 3350 8150 3350
Wire Wire Line
	8150 3350 8150 3550
Wire Wire Line
	8100 4100 8150 4100
Wire Wire Line
	7100 2950 7050 2950
Wire Wire Line
	7100 3500 7050 3500
Wire Wire Line
	7100 4100 7050 4100
Wire Wire Line
	8100 2950 8150 2950
Wire Wire Line
	8150 2950 8150 3350
Connection ~ 8150 3350
Text Label 8050 3650 2    50   ~ 0
VDDA
Wire Wire Line
	8050 3650 8150 3650
Wire Wire Line
	8150 3650 8150 3550
Text Notes 7600 2550 0    50   ~ 0
Analog Power Supply
Wire Notes Line
	7500 4450 6500 4450
Wire Notes Line
	7500 4450 7500 2450
Wire Notes Line
	7500 2450 6500 2450
Wire Notes Line
	6500 2450 6500 4450
Connection ~ 9850 4200
Wire Wire Line
	9850 4300 9850 4200
Wire Wire Line
	9100 4300 9850 4300
Connection ~ 9850 4050
Wire Wire Line
	9850 4200 9850 4050
Wire Wire Line
	9100 4200 9850 4200
Text Label 9100 4300 2    50   ~ 0
VDD3P3
Text Label 9100 4200 2    50   ~ 0
VDD3P3
Wire Wire Line
	9500 4050 8800 4050
Connection ~ 9150 3700
Wire Wire Line
	9350 3700 9150 3700
Wire Wire Line
	9850 3700 9850 4050
Connection ~ 9150 2900
Wire Wire Line
	9250 2900 9150 2900
$Comp
L power:+3V3 #PWR?
U 1 1 5F851F4D
P 9250 2900
AR Path="/5F851F4D" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F4D" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 9250 2750 50  0001 C CNN
F 1 "+3.3V" V 9250 3100 28  0000 C CNN
F 2 "" H 9250 2900 50  0000 C CNN
F 3 "" H 9250 2900 50  0000 C CNN
	1    9250 2900
	0    1    -1   0   
$EndComp
$Comp
L pspice:INDUCTOR L?
U 1 1 5F851F53
P 9600 3700
AR Path="/5F851F53" Ref="L?"  Part="1" 
AR Path="/60193DF4/5F851F53" Ref="L1"  Part="1" 
F 0 "L1" H 9600 3915 50  0000 C CNN
F 1 "2.0 uH" H 9600 3824 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 9600 3700 50  0001 C CNN
F 3 "~" H 9600 3700 50  0001 C CNN
	1    9600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3300 9150 3700
Wire Wire Line
	9800 4050 9850 4050
Wire Wire Line
	9150 3700 9100 3700
$Comp
L power:GND #PWR?
U 1 1 5F851F5C
P 8800 3700
AR Path="/5F851F5C" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F5C" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 8800 3450 50  0001 C CNN
F 1 "GND" H 8800 3550 50  0000 C CNN
F 2 "" H 8800 3700 50  0000 C CNN
F 3 "" H 8800 3700 50  0000 C CNN
	1    8800 3700
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851F62
P 8950 3700
AR Path="/5F851F62" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F62" Ref="C9"  Part="1" 
F 0 "C9" V 9202 3700 50  0000 C CNN
F 1 "0.1uF" V 9111 3700 50  0000 C CNN
F 2 "" H 8988 3550 50  0001 C CNN
F 3 "~" H 8950 3700 50  0001 C CNN
	1    8950 3700
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851F68
P 9650 4050
AR Path="/5F851F68" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F68" Ref="C10"  Part="1" 
F 0 "C10" V 9902 4050 50  0000 C CNN
F 1 "1 uF" V 9811 4050 50  0000 C CNN
F 2 "" H 9688 3900 50  0001 C CNN
F 3 "~" H 9650 4050 50  0001 C CNN
	1    9650 4050
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851F6E
P 8800 4050
AR Path="/5F851F6E" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F6E" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 8800 3800 50  0001 C CNN
F 1 "GND" H 8800 3900 50  0000 C CNN
F 2 "" H 8800 4050 50  0000 C CNN
F 3 "" H 8800 4050 50  0000 C CNN
	1    8800 4050
	0    1    -1   0   
$EndComp
Connection ~ 9150 3300
Wire Wire Line
	9100 3300 9150 3300
Wire Wire Line
	9150 2900 9100 2900
Wire Wire Line
	9150 2900 9150 3300
$Comp
L power:GND #PWR?
U 1 1 5F851F78
P 8800 2900
AR Path="/5F851F78" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F78" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 8800 2650 50  0001 C CNN
F 1 "GND" H 8800 2750 50  0000 C CNN
F 2 "" H 8800 2900 50  0000 C CNN
F 3 "" H 8800 2900 50  0000 C CNN
	1    8800 2900
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851F7E
P 8950 2900
AR Path="/5F851F7E" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F7E" Ref="C7"  Part="1" 
F 0 "C7" V 9202 2900 50  0000 C CNN
F 1 "10 uF" V 9111 2900 50  0000 C CNN
F 2 "" H 8988 2750 50  0001 C CNN
F 3 "~" H 8950 2900 50  0001 C CNN
	1    8950 2900
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F851F84
P 8950 3300
AR Path="/5F851F84" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F851F84" Ref="C8"  Part="1" 
F 0 "C8" V 9202 3300 50  0000 C CNN
F 1 "1 uF" V 9111 3300 50  0000 C CNN
F 2 "" H 8988 3150 50  0001 C CNN
F 3 "~" H 8950 3300 50  0001 C CNN
	1    8950 3300
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F851F8A
P 8800 3300
AR Path="/5F851F8A" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F851F8A" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 8800 3050 50  0001 C CNN
F 1 "GND" H 8800 3150 50  0000 C CNN
F 2 "" H 8800 3300 50  0000 C CNN
F 3 "" H 8800 3300 50  0000 C CNN
	1    8800 3300
	0    1    -1   0   
$EndComp
Wire Notes Line
	7550 2450 7550 4450
Wire Notes Line
	7550 4450 10050 4450
Wire Notes Line
	10050 4450 10050 2450
Wire Notes Line
	10050 2450 7550 2450
Wire Notes Line
	2900 600  2900 6450
Wire Notes Line
	2900 6450 600  6450
Wire Notes Line
	600  6450 600  600 
Wire Notes Line
	600  600  2900 600 
Text Notes 750  1750 0    50   ~ 0
Project Description\n\nDIY Hexacopter Project with a focus on:\n- using a super capacitor for energy storage\n- minimizing cost of drone\n- minimizing count of hand-soldered components\n\n\nFlight controller - Arduino\nCommunication - ESP32\nFrame - PCB\nMotor - Easton clone\nProp - Easton clone\n
Wire Notes Line
	2950 600  5250 600 
Wire Notes Line
	5250 4000 2950 4000
Wire Notes Line
	5250 600  5250 4000
Wire Notes Line
	2950 4000 2950 600 
$EndSCHEMATC
