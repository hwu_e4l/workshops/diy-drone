EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_FET:LM5109AMA U?
U 1 1 5F2D30E5
P 2500 1400
AR Path="/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/5F835A2C/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/601C7477/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/601CE793/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/601CE795/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30E5" Ref="U?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30E5" Ref="U10"  Part="1" 
F 0 "U10" H 2500 1967 50  0000 C CNN
F 1 "LM5109AMA" H 2500 1876 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2500 900 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm5109a.pdf" H 2500 1400 50  0001 C CNN
	1    2500 1400
	1    0    0    -1  
$EndComp
Text Label 1900 1100 2    50   ~ 0
VDD
Text Label 1900 1700 2    50   ~ 0
VSS
Text Label 3150 1100 0    50   ~ 0
SUPPLY
Text Label 3150 1500 0    50   ~ 0
HIGH_SIDE_GATE
Text Label 3150 1700 0    50   ~ 0
LOW_SIDE_GATE
Text Label 3150 1600 0    50   ~ 0
SOURCE
Wire Wire Line
	2800 1100 3150 1100
Wire Wire Line
	3150 1500 2800 1500
Wire Wire Line
	2800 1600 3150 1600
Wire Wire Line
	3150 1700 2800 1700
Wire Wire Line
	6300 3600 6200 3600
Text GLabel 6300 3600 2    50   Input ~ 0
MOTOR_F_POS
Wire Wire Line
	6200 3600 6200 3650
Connection ~ 6200 3600
$Comp
L power:GND #PWR?
U 1 1 5F7E4A1A
P 6200 4050
AR Path="/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A1A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A1A" Ref="#PWR0196"  Part="1" 
F 0 "#PWR0196" H 6200 3800 50  0001 C CNN
F 1 "GND" H 6200 3900 50  0000 C CNN
F 2 "" H 6200 4050 50  0000 C CNN
F 3 "" H 6200 4050 50  0000 C CNN
	1    6200 4050
	-1   0    0    -1  
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F7E4A20
P 6200 3150
AR Path="/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A20" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A20" Ref="#PWR0197"  Part="1" 
F 0 "#PWR0197" H 6200 3050 50  0001 C CNN
F 1 "+VDC" H 6200 3425 50  0000 C CNN
F 2 "" H 6200 3150 50  0001 C CNN
F 3 "" H 6200 3150 50  0001 C CNN
	1    6200 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3550 6200 3600
Text Label 5050 3600 2    50   ~ 0
SOURCE
Text Label 5050 3850 2    50   ~ 0
LOW_SIDE_GATE
Text Label 5050 3350 2    50   ~ 0
HIGH_SIDE_GATE
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F7E4A2A
P 6100 3850
AR Path="/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A2A" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A2A" Ref="Q24"  Part="1" 
F 0 "Q24" H 6307 3896 50  0000 L CNN
F 1 "TIP122" H 6307 3805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6300 3775 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 6100 3850 50  0001 L CNN
	1    6100 3850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F2D30E9
P 6100 3350
AR Path="/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30E9" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30E9" Ref="Q23"  Part="1" 
F 0 "Q23" H 6307 3396 50  0000 L CNN
F 1 "TIP122" H 6307 3305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6300 3275 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 6100 3350 50  0001 L CNN
	1    6100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3600 6200 3600
Wire Wire Line
	1900 5000 1700 5000
Text Label 1700 5000 2    50   ~ 0
SUPPLY
$Comp
L Device:R_Small_US R?
U 1 1 5F2D30EA
P 2550 5000
AR Path="/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/601C7477/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/601CE793/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/601CE795/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30EA" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30EA" Ref="R48"  Part="1" 
F 0 "R48" V 2755 5000 50  0000 C CNN
F 1 "1" V 2664 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2550 5000 50  0001 C CNN
F 3 "~" H 2550 5000 50  0001 C CNN
	1    2550 5000
	0    -1   -1   0   
$EndComp
Text Label 1150 1300 2    50   ~ 0
VSS
$Comp
L Device:C_Small C?
U 1 1 5F2D30EB
P 2200 6650
AR Path="/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/5F835A2C/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/601C7477/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/601CE793/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/601CE795/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30EB" Ref="C?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30EB" Ref="C36"  Part="1" 
F 0 "C36" V 2429 6650 50  0000 C CNN
F 1 "100 nF" V 2338 6650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 6650 50  0001 C CNN
F 3 "~" H 2200 6650 50  0001 C CNN
	1    2200 6650
	0    -1   -1   0   
$EndComp
Text Label 1600 6650 2    50   ~ 0
SUPPLY
Text Label 2850 6650 0    50   ~ 0
SOURCE
$Comp
L power:GND #PWR?
U 1 1 5F2D30EC
P 1300 1300
AR Path="/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30EC" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30EC" Ref="#PWR0198"  Part="1" 
F 0 "#PWR0198" H 1300 1050 50  0001 C CNN
F 1 "GND" H 1300 1150 50  0000 C CNN
F 2 "" H 1300 1300 50  0000 C CNN
F 3 "" H 1300 1300 50  0000 C CNN
	1    1300 1300
	0    -1   1    0   
$EndComp
Wire Wire Line
	1600 6650 2100 6650
Wire Wire Line
	2300 6650 2850 6650
Text GLabel 1650 1500 0    50   Input ~ 0
HBRIDGE_F_HIGH_INPUT
Text GLabel 1650 1600 0    50   Input ~ 0
HBRIDGE_F_LOW_INPUT
Wire Wire Line
	1900 1100 2200 1100
Wire Wire Line
	1900 1700 2200 1700
Wire Wire Line
	1650 1500 2200 1500
Wire Wire Line
	1650 1600 2200 1600
Wire Wire Line
	5000 2100 5100 2100
Text GLabel 5000 2100 0    50   Input ~ 0
MOTOR_F_NEG
Wire Wire Line
	5100 2100 5100 2150
Connection ~ 5100 2100
Wire Wire Line
	5400 1850 5500 1850
Wire Wire Line
	5500 1550 5500 1850
Wire Wire Line
	5550 1550 5500 1550
$Comp
L Device:R_Small_US R?
U 1 1 5F2D30ED
P 5700 1850
AR Path="/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/601C7477/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/601CE793/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/601CE795/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30ED" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30ED" Ref="R51"  Part="1" 
F 0 "R51" V 5495 1850 50  0000 C CNN
F 1 "4.7" V 5586 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 1850 50  0001 C CNN
F 3 "~" H 5700 1850 50  0001 C CNN
	1    5700 1850
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7E4A63
P 5100 2550
AR Path="/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A63" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A63" Ref="#PWR0199"  Part="1" 
F 0 "#PWR0199" H 5100 2300 50  0001 C CNN
F 1 "GND" H 5100 2400 50  0000 C CNN
F 2 "" H 5100 2550 50  0000 C CNN
F 3 "" H 5100 2550 50  0000 C CNN
	1    5100 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F2D30EF
P 5100 1650
AR Path="/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30EF" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30EF" Ref="#PWR0200"  Part="1" 
F 0 "#PWR0200" H 5100 1550 50  0001 C CNN
F 1 "+VDC" H 5100 1925 50  0000 C CNN
F 2 "" H 5100 1650 50  0001 C CNN
F 3 "" H 5100 1650 50  0001 C CNN
	1    5100 1650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 2050 5100 2100
Text Label 6250 2100 0    50   ~ 0
SOURCE
Text Label 6250 1850 0    50   ~ 0
LOW_SIDE_GATE
Text Label 6250 2350 0    50   ~ 0
HIGH_SIDE_GATE
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F2D30F0
P 5200 2350
AR Path="/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30F0" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30F0" Ref="Q22"  Part="1" 
F 0 "Q22" H 5407 2396 50  0000 L CNN
F 1 "TIP122" H 5407 2305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 2275 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5200 2350 50  0001 L CNN
	1    5200 2350
	-1   0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP122 Q?
U 1 1 5F2D30F1
P 5200 1850
AR Path="/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/5F835A2C/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/601C7477/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/601CE793/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/601CE795/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30F1" Ref="Q?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30F1" Ref="Q21"  Part="1" 
F 0 "Q21" H 5407 1896 50  0000 L CNN
F 1 "TIP122" H 5407 1805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5400 1775 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5200 1850 50  0001 L CNN
	1    5200 1850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6250 2100 5100 2100
Text GLabel 5700 5150 0    50   Input ~ 0
MOTOR_F_POS
Text GLabel 5700 5400 0    50   Input ~ 0
MOTOR_F_NEG
$Comp
L Connector:TestPoint TP?
U 1 1 5F2D30F2
P 5700 5150
AR Path="/602705E1/60274500/5F2D30F2" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30F2" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30F2" Ref="TP17"  Part="1" 
F 0 "TP17" V 5654 5338 50  0000 L CNN
F 1 "TestPoint" V 5745 5338 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 5900 5150 50  0001 C CNN
F 3 "~" H 5900 5150 50  0001 C CNN
	1    5700 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5F7E4A88
P 5700 5400
AR Path="/602705E1/60274500/5F7E4A88" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A88" Ref="TP?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A88" Ref="TP18"  Part="1" 
F 0 "TP18" V 5654 5588 50  0000 L CNN
F 1 "TestPoint" V 5745 5588 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D4.0mm" H 5900 5400 50  0001 C CNN
F 3 "~" H 5900 5400 50  0001 C CNN
	1    5700 5400
	0    1    1    0   
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 5F7E4A8E
P 5450 5850
AR Path="/602705E1/60274500/5F7E4A8E" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A8E" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A8E" Ref="D35"  Part="1" 
F 0 "D35" H 5443 5595 50  0000 C CNN
F 1 "LED_ALT" H 5443 5686 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5450 5850 50  0001 C CNN
F 3 "~" H 5450 5850 50  0001 C CNN
	1    5450 5850
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5F7E4A94
P 5950 5850
AR Path="/602705E1/60274500/5F7E4A94" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A94" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A94" Ref="R53"  Part="1" 
F 0 "R53" V 5745 5850 50  0000 C CNN
F 1 "10k" V 5836 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 5850 50  0001 C CNN
F 3 "~" H 5950 5850 50  0001 C CNN
	1    5950 5850
	0    1    1    0   
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5F7E4A9A
P 5050 5850
AR Path="/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4A9A" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4A9A" Ref="#PWR0201"  Part="1" 
F 0 "#PWR0201" H 5050 5750 50  0001 C CNN
F 1 "+VDC" H 5050 6125 50  0000 C CNN
F 2 "" H 5050 5850 50  0001 C CNN
F 3 "" H 5050 5850 50  0001 C CNN
	1    5050 5850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7E4AA0
P 6300 5850
AR Path="/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/5F835A2C/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/601C7477/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/601CE793/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/601CE795/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274500/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4AA0" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4AA0" Ref="#PWR0202"  Part="1" 
F 0 "#PWR0202" H 6300 5600 50  0001 C CNN
F 1 "GND" H 6300 5700 50  0000 C CNN
F 2 "" H 6300 5850 50  0000 C CNN
F 3 "" H 6300 5850 50  0000 C CNN
	1    6300 5850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 5850 5300 5850
Wire Wire Line
	5600 5850 5850 5850
Wire Wire Line
	6050 5850 6300 5850
Wire Notes Line
	4150 550  7150 550 
Wire Notes Line
	4150 4550 7150 4550
Wire Notes Line
	4150 4650 4150 6150
Text Notes 4200 4750 0    50   ~ 0
Status Flags
Wire Notes Line
	4700 4650 4700 4800
Wire Notes Line
	4700 4800 4150 4800
Wire Notes Line
	7150 550  7150 4550
Wire Notes Line
	4150 550  4150 4550
Text Notes 4200 650  0    50   ~ 0
H-Bridge 
Wire Notes Line
	4600 550  4600 700 
Wire Notes Line
	4600 700  4150 700 
Wire Notes Line
	4050 550  550  550 
Text Notes 1050 3450 0    50   ~ 0
The LM5109 is a  low cost high voltage gate driver,\ndesigned to drive both the high side and the low side\nN-Channel MOSFETs in a synchronous buck or a\nhalf bridge configuration. The floating high-side driver\nis capable of working with rail voltages up to 100V.\n\nThe outputs are independently controlled with TTL\ncompatible input thresholds. A robust level shifter\ntechnology operates at high speed while consuming\nlow power and providing clean level transitions from\nthe control input logic to the high side gate driver.\n\nUnder-voltage lockout is provided on both the low\nside and the high side power rails.
Text Notes 1050 2250 0    118  ~ 0
Description
Wire Notes Line
	3500 2050 1000 2050
Text Notes 1050 3750 0    50   ~ 0
Bootstrap Diode and Resistor
Text Label 2950 5000 0    50   ~ 0
VDD
Text Label 1100 1050 2    50   ~ 0
VDD
Text Notes 1100 6350 0    50   ~ 0
The bootstrap capacitor must maintain the voltage \ndifference between HB and HS voltage above the \nUVLO threshold for normal operation.\n\nNot gonna bother with the calculations as a \n100 nF capacitor is plenty enough to store the \ncharge required by a bootstrap cap.
Wire Notes Line
	1000 3650 3500 3650
$Comp
L power:+3.3V #PWR?
U 1 1 5F2D30F8
P 1300 1050
AR Path="/602705E1/60274500/5F2D30F8" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30F8" Ref="#PWR?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30F8" Ref="#PWR0203"  Part="1" 
F 0 "#PWR0203" H 1300 900 50  0001 C CNN
F 1 "+3.3V" H 1315 1223 50  0000 C CNN
F 2 "" H 1300 1050 50  0001 C CNN
F 3 "" H 1300 1050 50  0001 C CNN
	1    1300 1050
	0    1    1    0   
$EndComp
Text Notes 1100 4800 0    50   ~ 0
Many half-bridge drivers incorporate a bootstrap\ndiode to generate high-side bias, reducing the \nboard space and component count. In high frequency\nand capacitive load applications, it is beneficial \nto add an external bootstrap diode to reduce losses.\n\nGeneral requirements:\n- Reverse recovery time of the bootstrap diode must \n  be very small, to reduce reverse recovery losses.\n- Low forward voltage drop (to reduce the size of \n  bootstrap capacitor).\n\n
$Comp
L Diode:1N4148WS D?
U 1 1 5F7E4AC4
P 2050 5000
AR Path="/602705E1/60274500/5F7E4AC4" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F7E4AC4" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F7E4AC4" Ref="D34"  Part="1" 
F 0 "D34" H 2050 5217 50  0000 C CNN
F 1 "1N4148WS" H 2050 5126 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 2050 4825 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 2050 5000 50  0001 C CNN
	1    2050 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5000 2450 5000
Wire Wire Line
	2650 5000 2950 5000
Text Notes 1100 5500 0    50   ~ 0
The bootstrap diode selected has a forward drop \nof about 1.0 V and a maximum forward surge \ncurrent of 2 A.  The bootstrap resistor should \nreduce the inrush current to acceptable values. \n\n
Wire Notes Line
	1000 5500 3500 5500
Wire Notes Line
	3500 3650 3500 5500
Wire Notes Line
	1000 3650 1000 5500
Wire Notes Line
	1000 2050 1000 3550
Wire Notes Line
	1000 3550 3500 3550
Wire Notes Line
	3500 3550 3500 2050
Text Notes 1050 5700 0    50   ~ 0
Bootstrap and Bypass Capacitor
Wire Notes Line
	1000 5600 3500 5600
Wire Notes Line
	3500 5600 3500 7450
Wire Notes Line
	1000 5600 1000 7450
Text Label 1550 7250 2    50   ~ 0
VDD
Text Label 2850 7250 0    50   ~ 0
VSS
$Comp
L Device:C_Small C?
U 1 1 5F2D30FA
P 2200 7250
AR Path="/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/5F835A2C/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/601C7477/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/601CE793/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/601CE795/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FA" Ref="C?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FA" Ref="C37"  Part="1" 
F 0 "C37" V 2429 7250 50  0000 C CNN
F 1 "1 uF" V 2338 7250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 7250 50  0001 C CNN
F 3 "~" H 2200 7250 50  0001 C CNN
	1    2200 7250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 7250 2850 7250
Wire Wire Line
	1550 7250 2100 7250
Text Notes 1100 7000 0    50   ~ 0
The bypass capacitor filters any AC noise in a \nDC signal. Should be about ten times the bootstrap\ncapacitor value at least. 
Wire Notes Line
	1000 7450 3500 7450
Wire Notes Line
	1000 3800 2200 3800
Wire Notes Line
	2200 3800 2200 3650
Wire Notes Line
	1000 5750 2300 5750
Wire Notes Line
	2300 5750 2300 5600
Wire Wire Line
	1150 1300 1300 1300
Wire Wire Line
	1100 1050 1300 1050
$Comp
L Diode:1N4148WS D?
U 1 1 5F2D30FB
P 5700 1550
AR Path="/602705E1/60274500/5F2D30FB" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FB" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FB" Ref="D38"  Part="1" 
F 0 "D38" H 5700 1333 50  0000 C CNN
F 1 "1N4148WS" H 5700 1424 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 1375 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5700 1550 50  0001 C CNN
	1    5700 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 1550 5900 1550
Wire Wire Line
	5900 1550 5900 1850
Wire Wire Line
	5900 1850 6250 1850
Wire Wire Line
	5500 1850 5600 1850
Connection ~ 5500 1850
Wire Wire Line
	5900 1850 5800 1850
Connection ~ 5900 1850
Wire Wire Line
	5400 2350 5500 2350
Wire Wire Line
	5500 2650 5500 2350
Wire Wire Line
	5550 2650 5500 2650
$Comp
L Device:R_Small_US R?
U 1 1 5F2D30FC
P 5700 2350
AR Path="/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/601C7477/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/601CE793/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/601CE795/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FC" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FC" Ref="R52"  Part="1" 
F 0 "R52" V 5495 2350 50  0000 C CNN
F 1 "4.7" V 5586 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 2350 50  0001 C CNN
F 3 "~" H 5700 2350 50  0001 C CNN
	1    5700 2350
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F2D30FD
P 5700 2650
AR Path="/602705E1/60274500/5F2D30FD" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FD" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FD" Ref="D39"  Part="1" 
F 0 "D39" H 5700 2867 50  0000 C CNN
F 1 "1N4148WS" H 5700 2776 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 2475 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5700 2650 50  0001 C CNN
	1    5700 2650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 2650 5900 2650
Wire Wire Line
	5900 2650 5900 2350
Wire Wire Line
	5900 2350 6250 2350
Wire Wire Line
	5500 2350 5600 2350
Connection ~ 5500 2350
Wire Wire Line
	5900 2350 5800 2350
Connection ~ 5900 2350
Wire Wire Line
	5900 3350 5800 3350
Wire Wire Line
	5800 3050 5800 3350
Wire Wire Line
	5750 3050 5800 3050
$Comp
L Device:R_Small_US R?
U 1 1 5F2D30FE
P 5600 3350
AR Path="/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/601C7477/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/601CE793/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/601CE795/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FE" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FE" Ref="R49"  Part="1" 
F 0 "R49" V 5395 3350 50  0000 C CNN
F 1 "4.7" V 5486 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3350 50  0001 C CNN
F 3 "~" H 5600 3350 50  0001 C CNN
	1    5600 3350
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F2D30FF
P 5600 3050
AR Path="/602705E1/60274500/5F2D30FF" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F2D30FF" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F2D30FF" Ref="D36"  Part="1" 
F 0 "D36" H 5600 2833 50  0000 C CNN
F 1 "1N4148WS" H 5600 2924 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5600 2875 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5600 3050 50  0001 C CNN
	1    5600 3050
	1    0    0    1   
$EndComp
Wire Wire Line
	5450 3050 5400 3050
Wire Wire Line
	5400 3050 5400 3350
Wire Wire Line
	5400 3350 5050 3350
Wire Wire Line
	5800 3350 5700 3350
Connection ~ 5800 3350
Wire Wire Line
	5400 3350 5500 3350
Connection ~ 5400 3350
Wire Wire Line
	5900 3850 5800 3850
Wire Wire Line
	5800 4150 5800 3850
Wire Wire Line
	5750 4150 5800 4150
$Comp
L Device:R_Small_US R?
U 1 1 5F2D3100
P 5600 3850
AR Path="/5F2D3100" Ref="R?"  Part="1" 
AR Path="/5F835A2C/5F2D3100" Ref="R?"  Part="1" 
AR Path="/601C7477/5F2D3100" Ref="R?"  Part="1" 
AR Path="/601CE793/5F2D3100" Ref="R?"  Part="1" 
AR Path="/601CE795/5F2D3100" Ref="R?"  Part="1" 
AR Path="/602705E1/60274500/5F2D3100" Ref="R?"  Part="1" 
AR Path="/602705E1/60274504/5F2D3100" Ref="R?"  Part="1" 
AR Path="/602705E1/60274506/5F2D3100" Ref="R50"  Part="1" 
F 0 "R50" V 5395 3850 50  0000 C CNN
F 1 "4.7" V 5486 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3850 50  0001 C CNN
F 3 "~" H 5600 3850 50  0001 C CNN
	1    5600 3850
	0    -1   1    0   
$EndComp
$Comp
L Diode:1N4148WS D?
U 1 1 5F2D3101
P 5600 4150
AR Path="/602705E1/60274500/5F2D3101" Ref="D?"  Part="1" 
AR Path="/602705E1/60274504/5F2D3101" Ref="D?"  Part="1" 
AR Path="/602705E1/60274506/5F2D3101" Ref="D37"  Part="1" 
F 0 "D37" H 5600 4367 50  0000 C CNN
F 1 "1N4148WS" H 5600 4276 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5600 3975 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85751/1n4148ws.pdf" H 5600 4150 50  0001 C CNN
	1    5600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4150 5400 4150
Wire Wire Line
	5400 4150 5400 3850
Wire Wire Line
	5400 3850 5050 3850
Wire Wire Line
	5800 3850 5700 3850
Connection ~ 5800 3850
Wire Wire Line
	5400 3850 5500 3850
Connection ~ 5400 3850
Wire Notes Line
	4050 550  4050 7750
Wire Notes Line
	550  550  550  7750
Text Notes 600  650  0    50   ~ 0
H-Bridge Driver
Wire Notes Line
	550  700  1250 700 
Wire Notes Line
	1250 700  1250 550 
Wire Notes Line
	7150 4650 7150 6150
Wire Notes Line
	4150 6150 7150 6150
Wire Notes Line
	4150 4650 7150 4650
Wire Notes Line
	550  7750 4050 7750
$EndSCHEMATC
