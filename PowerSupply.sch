EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5827 8268
encoding utf-8
Sheet 14 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_PMIC-LED-Drivers:TPS61040DBVR U?
U 1 1 60298B33
P 2550 3500
AR Path="/60298B33" Ref="U?"  Part="1" 
AR Path="/601C2277/60298B33" Ref="U11"  Part="1" 
F 0 "U11" H 2800 3500 60  0000 L CNN
F 1 "TPS61040DBVR" H 2800 3400 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 2750 3700 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Ftps61041" H 2750 3800 60  0001 L CNN
F 4 "296-12685-1-ND" H 2750 3900 60  0001 L CNN "Digi-Key_PN"
F 5 "TPS61040DBVR" H 2750 4000 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2750 4100 60  0001 L CNN "Category"
F 7 "PMIC - LED Drivers" H 2750 4200 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Ftps61041" H 2750 4300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/TPS61040DBVR/296-12685-1-ND/454262" H 2750 4400 60  0001 L CNN "DK_Detail_Page"
F 10 "IC LED DRIVER RGLTR DIM SOT23-5" H 2750 4500 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 2750 4600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2750 4700 60  0001 L CNN "Status"
	1    2550 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60298B39
P 2000 6150
AR Path="/60298B39" Ref="C?"  Part="1" 
AR Path="/601C2277/60298B39" Ref="C38"  Part="1" 
F 0 "C38" V 1771 6150 50  0000 C CNN
F 1 "4.7 uF" V 1862 6150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2000 6150 50  0001 C CNN
F 3 "~" H 2000 6150 50  0001 C CNN
	1    2000 6150
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60298B3F
P 2350 4550
AR Path="/60298B3F" Ref="R?"  Part="1" 
AR Path="/601C2277/60298B3F" Ref="R54"  Part="1" 
F 0 "R54" V 2145 4550 50  0000 C CNN
F 1 "R2" V 2236 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2350 4550 50  0001 C CNN
F 3 "~" H 2350 4550 50  0001 C CNN
	1    2350 4550
	0    -1   1    0   
$EndComp
$Comp
L Device:D_Small_ALT D?
U 1 1 60298B45
P 2500 4950
AR Path="/60298B45" Ref="D?"  Part="1" 
AR Path="/601C2277/60298B45" Ref="D40"  Part="1" 
F 0 "D40" H 2500 4745 50  0000 C CNN
F 1 "D1" H 2500 4836 50  0000 C CNN
F 2 "" V 2500 4950 50  0001 C CNN
F 3 "~" V 2500 4950 50  0001 C CNN
	1    2500 4950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60298B4B
P 2550 3800
AR Path="/60298B4B" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B4B" Ref="#PWR0204"  Part="1" 
F 0 "#PWR0204" H 2550 3550 50  0001 C CNN
F 1 "GND" H 2555 3627 50  0000 C CNN
F 2 "" H 2550 3800 50  0001 C CNN
F 3 "" H 2550 3800 50  0001 C CNN
	1    2550 3800
	1    0    0    -1  
$EndComp
Text Label 2250 3400 2    50   ~ 0
STEPUP_ENABLE
Text Label 2250 3500 2    50   ~ 0
STEPUP_FEEDBACK
Text Label 2850 3400 0    50   ~ 0
STEPUP_SWITCH
$Comp
L Device:C_Small C?
U 1 1 60298B54
P 2500 5150
AR Path="/60298B54" Ref="C?"  Part="1" 
AR Path="/601C2277/60298B54" Ref="C39"  Part="1" 
F 0 "C39" H 2592 5196 50  0000 L CNN
F 1 "1 uF" H 2592 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2500 5150 50  0001 C CNN
F 3 "~" H 2500 5150 50  0001 C CNN
	1    2500 5150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60298B5A
P 2250 4550
AR Path="/60298B5A" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B5A" Ref="#PWR0205"  Part="1" 
F 0 "#PWR0205" H 2250 4300 50  0001 C CNN
F 1 "GND" H 2255 4377 50  0000 C CNN
F 2 "" H 2250 4550 50  0001 C CNN
F 3 "" H 2250 4550 50  0001 C CNN
	1    2250 4550
	0    1    -1   0   
$EndComp
Text Label 1950 5550 0    50   ~ 0
STEPUP_ENABLE
$Comp
L Device:L_Small L?
U 1 1 60298B61
P 2000 5850
AR Path="/60298B61" Ref="L?"  Part="1" 
AR Path="/601C2277/60298B61" Ref="L2"  Part="1" 
F 0 "L2" V 1819 5850 50  0000 C CNN
F 1 "10 uH" V 1910 5850 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 2000 5850 50  0001 C CNN
F 3 "~" H 2000 5850 50  0001 C CNN
	1    2000 5850
	0    -1   1    0   
$EndComp
Text Label 2200 5850 0    50   ~ 0
STEPUP_SWITCH
Text Label 2250 4950 2    50   ~ 0
STEPUP_SWITCH
$Comp
L Device:R_Small_US R?
U 1 1 60298B69
P 2700 4200
AR Path="/60298B69" Ref="R?"  Part="1" 
AR Path="/601C2277/60298B69" Ref="R55"  Part="1" 
F 0 "R55" V 2905 4200 50  0000 C CNN
F 1 "R1" V 2814 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2700 4200 50  0001 C CNN
F 3 "~" H 2700 4200 50  0001 C CNN
	1    2700 4200
	0    1    -1   0   
$EndComp
Text Label 2450 4200 2    50   ~ 0
STEPUP_FEEDBACK
$Comp
L Device:C_Small C?
U 1 1 60298B70
P 2700 4550
AR Path="/60298B70" Ref="C?"  Part="1" 
AR Path="/601C2277/60298B70" Ref="C40"  Part="1" 
F 0 "C40" V 2471 4550 50  0000 C CNN
F 1 "CFF" V 2562 4550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2700 4550 50  0001 C CNN
F 3 "~" H 2700 4550 50  0001 C CNN
	1    2700 4550
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 4200 2850 4200
Wire Wire Line
	2850 4200 2850 4550
Wire Wire Line
	2850 4550 2800 4550
Wire Wire Line
	2600 4550 2550 4550
Wire Wire Line
	2550 4550 2550 4200
Wire Wire Line
	2550 4200 2600 4200
Connection ~ 2850 4200
$Comp
L power:+BATT #PWR?
U 1 1 60298B7D
P 2550 3200
AR Path="/60298B7D" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B7D" Ref="#PWR0206"  Part="1" 
F 0 "#PWR0206" H 2550 3050 50  0001 C CNN
F 1 "+BATT" H 2565 3373 50  0000 C CNN
F 2 "" H 2550 3200 50  0001 C CNN
F 3 "" H 2550 3200 50  0001 C CNN
	1    2550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 60298B83
P 1800 5500
AR Path="/60298B83" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B83" Ref="#PWR0207"  Part="1" 
F 0 "#PWR0207" H 1800 5350 50  0001 C CNN
F 1 "+BATT" H 1815 5673 50  0000 C CNN
F 2 "" H 1800 5500 50  0001 C CNN
F 3 "" H 1800 5500 50  0001 C CNN
	1    1800 5500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 5500 1800 5550
Wire Wire Line
	1800 5550 1950 5550
Wire Wire Line
	1800 5550 1800 5850
Wire Wire Line
	1800 5850 1900 5850
Connection ~ 1800 5550
Wire Wire Line
	2100 5850 2200 5850
Wire Wire Line
	1800 5850 1800 6150
Wire Wire Line
	1800 6150 1900 6150
Connection ~ 1800 5850
$Comp
L power:GND #PWR?
U 1 1 60298B92
P 2100 6150
AR Path="/60298B92" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B92" Ref="#PWR0208"  Part="1" 
F 0 "#PWR0208" H 2100 5900 50  0001 C CNN
F 1 "GND" H 2105 5977 50  0000 C CNN
F 2 "" H 2100 6150 50  0001 C CNN
F 3 "" H 2100 6150 50  0001 C CNN
	1    2100 6150
	0    -1   1    0   
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 60298B98
P 2750 4950
AR Path="/60298B98" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B98" Ref="#PWR0209"  Part="1" 
F 0 "#PWR0209" H 2750 4850 50  0001 C CNN
F 1 "+VDC" H 2750 5225 50  0000 C CNN
F 2 "" H 2750 4950 50  0001 C CNN
F 3 "" H 2750 4950 50  0001 C CNN
	1    2750 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 4950 2650 4950
$Comp
L power:GND #PWR?
U 1 1 60298B9F
P 2400 5150
AR Path="/60298B9F" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298B9F" Ref="#PWR0210"  Part="1" 
F 0 "#PWR0210" H 2400 4900 50  0001 C CNN
F 1 "GND" H 2405 4977 50  0000 C CNN
F 2 "" H 2400 5150 50  0001 C CNN
F 3 "" H 2400 5150 50  0001 C CNN
	1    2400 5150
	0    1    1    0   
$EndComp
Wire Notes Line
	1450 2850 1450 6350
$Comp
L power:+VDC #PWR?
U 1 1 60298BA6
P 3000 4200
AR Path="/60298BA6" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298BA6" Ref="#PWR0211"  Part="1" 
F 0 "#PWR0211" H 3000 4100 50  0001 C CNN
F 1 "+VDC" H 3000 4475 50  0000 C CNN
F 2 "" H 3000 4200 50  0001 C CNN
F 3 "" H 3000 4200 50  0001 C CNN
	1    3000 4200
	0    1    -1   0   
$EndComp
Wire Wire Line
	3000 4200 2850 4200
Wire Wire Line
	2600 5150 2650 5150
Wire Wire Line
	2650 5150 2650 4950
Connection ~ 2650 4950
Wire Wire Line
	2650 4950 2750 4950
Wire Wire Line
	2250 4950 2400 4950
Wire Wire Line
	2450 4200 2550 4200
Connection ~ 2550 4200
Wire Wire Line
	2550 4550 2450 4550
Connection ~ 2550 4550
$Comp
L power:+VDC #PWR?
U 1 1 60298BB6
P 3700 3300
AR Path="/60298BB6" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298BB6" Ref="#PWR0212"  Part="1" 
F 0 "#PWR0212" H 3700 3200 50  0001 C CNN
F 1 "+VDC" H 3700 3575 50  0000 C CNN
F 2 "" H 3700 3300 50  0001 C CNN
F 3 "" H 3700 3300 50  0001 C CNN
	1    3700 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 60298BBC
P 4000 3450
AR Path="/60298BBC" Ref="D?"  Part="1" 
AR Path="/601C2277/60298BBC" Ref="D41"  Part="1" 
F 0 "D41" H 3993 3195 50  0000 C CNN
F 1 "LED_ALT" H 3993 3286 50  0000 C CNN
F 2 "" H 4000 3450 50  0001 C CNN
F 3 "~" H 4000 3450 50  0001 C CNN
	1    4000 3450
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 60298BC2
P 4000 3900
AR Path="/60298BC2" Ref="D?"  Part="1" 
AR Path="/601C2277/60298BC2" Ref="D42"  Part="1" 
F 0 "D42" H 3993 3645 50  0000 C CNN
F 1 "LED_ALT" H 3993 3736 50  0000 C CNN
F 2 "" H 4000 3900 50  0001 C CNN
F 3 "~" H 4000 3900 50  0001 C CNN
	1    4000 3900
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 60298BC8
P 4000 4350
AR Path="/60298BC8" Ref="D?"  Part="1" 
AR Path="/601C2277/60298BC8" Ref="D43"  Part="1" 
F 0 "D43" H 3993 4095 50  0000 C CNN
F 1 "LED_ALT" H 3993 4186 50  0000 C CNN
F 2 "" H 4000 4350 50  0001 C CNN
F 3 "~" H 4000 4350 50  0001 C CNN
	1    4000 4350
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_ALT D?
U 1 1 60298BCE
P 4000 4750
AR Path="/60298BCE" Ref="D?"  Part="1" 
AR Path="/601C2277/60298BCE" Ref="D44"  Part="1" 
F 0 "D44" H 3993 4495 50  0000 C CNN
F 1 "LED_ALT" H 3993 4586 50  0000 C CNN
F 2 "" H 4000 4750 50  0001 C CNN
F 3 "~" H 4000 4750 50  0001 C CNN
	1    4000 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60298BD4
P 4400 3450
AR Path="/60298BD4" Ref="R?"  Part="1" 
AR Path="/601C2277/60298BD4" Ref="R56"  Part="1" 
F 0 "R56" V 4195 3450 50  0000 C CNN
F 1 "10K" V 4286 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 3450 50  0001 C CNN
F 3 "~" H 4400 3450 50  0001 C CNN
	1    4400 3450
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60298BDA
P 4400 3900
AR Path="/60298BDA" Ref="R?"  Part="1" 
AR Path="/601C2277/60298BDA" Ref="R57"  Part="1" 
F 0 "R57" V 4195 3900 50  0000 C CNN
F 1 "10K" V 4286 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 3900 50  0001 C CNN
F 3 "~" H 4400 3900 50  0001 C CNN
	1    4400 3900
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60298BE0
P 4400 4350
AR Path="/60298BE0" Ref="R?"  Part="1" 
AR Path="/601C2277/60298BE0" Ref="R58"  Part="1" 
F 0 "R58" V 4195 4350 50  0000 C CNN
F 1 "10K" V 4286 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 4350 50  0001 C CNN
F 3 "~" H 4400 4350 50  0001 C CNN
	1    4400 4350
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60298BE6
P 4400 4750
AR Path="/60298BE6" Ref="R?"  Part="1" 
AR Path="/601C2277/60298BE6" Ref="R59"  Part="1" 
F 0 "R59" V 4195 4750 50  0000 C CNN
F 1 "10K" V 4286 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4400 4750 50  0001 C CNN
F 3 "~" H 4400 4750 50  0001 C CNN
	1    4400 4750
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60298BEC
P 4800 4950
AR Path="/60298BEC" Ref="#PWR?"  Part="1" 
AR Path="/601C2277/60298BEC" Ref="#PWR0213"  Part="1" 
F 0 "#PWR0213" H 4800 4700 50  0001 C CNN
F 1 "GND" H 4800 4800 50  0000 C CNN
F 2 "" H 4800 4950 50  0000 C CNN
F 3 "" H 4800 4950 50  0000 C CNN
	1    4800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3300 3700 3450
Wire Wire Line
	3700 3450 3850 3450
Wire Wire Line
	3700 3450 3700 3900
Wire Wire Line
	3700 3900 3850 3900
Connection ~ 3700 3450
Wire Wire Line
	3850 4350 3700 4350
Wire Wire Line
	3700 4350 3700 3900
Connection ~ 3700 3900
Wire Wire Line
	3850 4750 3700 4750
Wire Wire Line
	3700 4750 3700 4350
Connection ~ 3700 4350
Wire Wire Line
	4800 4950 4800 4750
Wire Wire Line
	4500 3450 4800 3450
Wire Wire Line
	4500 3900 4800 3900
Connection ~ 4800 3900
Wire Wire Line
	4800 3900 4800 3450
Wire Wire Line
	4500 4350 4800 4350
Connection ~ 4800 4350
Wire Wire Line
	4800 4350 4800 3900
Wire Wire Line
	4500 4750 4800 4750
Connection ~ 4800 4750
Wire Wire Line
	4800 4750 4800 4350
Wire Wire Line
	4150 3450 4300 3450
Wire Wire Line
	4150 3900 4300 3900
Wire Wire Line
	4150 4350 4300 4350
Wire Wire Line
	4150 4750 4300 4750
Wire Notes Line
	5200 6350 5200 2850
Wire Notes Line
	1450 6350 5200 6350
Wire Notes Line
	1450 2850 5200 2850
$EndSCHEMATC
