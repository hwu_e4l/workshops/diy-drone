EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_RF-Transceiver-Modules:ESP32-WROOM-32 MOD?
U 1 1 60362920
P 1400 2000
AR Path="/60362920" Ref="MOD?"  Part="1" 
AR Path="/60193DF4/60362920" Ref="MOD?"  Part="1" 
AR Path="/60193DF4/6035E6AC/60362920" Ref="MOD1"  Part="1" 
F 0 "MOD1" H 1600 2303 60  0000 C CNN
F 1 "ESP32-WROOM-32" H 1600 2197 60  0000 C CNN
F 2 "digikey-footprints:ESP32-WROOM-32D" H 1600 2200 60  0001 L CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 1600 2300 60  0001 L CNN
F 4 "1904-1010-1-ND" H 1600 2400 60  0001 L CNN "Digi-Key_PN"
F 5 "ESP32-WROOM-32" H 1600 2500 60  0001 L CNN "MPN"
F 6 "RF/IF and RFID" H 1600 2600 60  0001 L CNN "Category"
F 7 "RF Transceiver Modules" H 1600 2700 60  0001 L CNN "Family"
F 8 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 1600 2800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/espressif-systems/ESP32-WROOM-32/1904-1010-1-ND/8544305" H 1600 2900 60  0001 L CNN "DK_Detail_Page"
F 10 "SMD MODULE, ESP32-D0WDQ6, 32MBIT" H 1600 3000 60  0001 L CNN "Description"
F 11 "Espressif Systems" H 1600 3100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1600 3200 60  0001 L CNN "Status"
	1    1400 2000
	1    0    0    -1  
$EndComp
Text GLabel 2300 3000 2    50   Input ~ 0
TX
Text GLabel 2300 2900 2    50   Input ~ 0
RX
$EndSCHEMATC
