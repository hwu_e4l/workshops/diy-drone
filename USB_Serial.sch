EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 14
Title ""
Date "2020-08-06"
Rev ""
Comp "grokkingStuff (Vishakh Kumar)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Interface-Controllers:FT232RL-REEL U?
U 1 1 5F84D553
P 2250 1750
AR Path="/5F84D553" Ref="U?"  Part="1" 
AR Path="/60193DF4/5F84D553" Ref="U?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D553" Ref="U3"  Part="1" 
F 0 "U3" H 2250 2300 60  0000 C CNN
F 1 "FT232RL-REEL" H 2500 2200 60  0000 C CNN
F 2 "digikey-footprints:SSOP-28_W5.30mm" H 2450 1950 60  0001 L CNN
F 3 "https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232R.pdf" H 2450 2050 60  0001 L CNN
F 4 "768-1007-1-ND" H 2450 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "FT232RL-REEL" H 2450 2250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2450 2350 60  0001 L CNN "Category"
F 7 "Interface - Controllers" H 2450 2450 60  0001 L CNN "Family"
F 8 "https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232R.pdf" H 2450 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ftdi-future-technology-devices-international-ltd/FT232RL-REEL/768-1007-1-ND/1836402" H 2450 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC USB FS SERIAL UART 28-SSOP" H 2450 2750 60  0001 L CNN "Description"
F 11 "FTDI, Future Technology Devices International Ltd" H 2450 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2450 2950 60  0001 L CNN "Status"
	1    2250 1750
	1    0    0    -1  
$EndComp
Text Label 2650 1650 0    50   ~ 0
USB_3V3_OUT
Text GLabel 2650 1850 2    50   Input ~ 0
TX
Text GLabel 1550 1850 0    50   Input ~ 0
RX
Text Label 2150 1250 1    50   ~ 0
USB_VCC
Text Label 2050 1250 1    50   ~ 0
USB_LOGIC
Wire Wire Line
	2050 2950 2050 3000
Wire Wire Line
	2050 3000 2150 3000
Wire Wire Line
	2450 3000 2450 2950
Wire Wire Line
	2350 2950 2350 3000
Wire Wire Line
	2350 3000 2450 3000
Wire Wire Line
	2250 2950 2250 3000
Wire Wire Line
	2150 2950 2150 3000
Connection ~ 2150 3000
Wire Wire Line
	2150 3000 2250 3000
$Comp
L power:GND #PWR?
U 1 1 5F84D56C
P 2150 3100
AR Path="/5F84D56C" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F84D56C" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D56C" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 2150 2850 50  0001 C CNN
F 1 "GND" H 2155 2927 50  0000 C CNN
F 2 "" H 2150 3100 50  0001 C CNN
F 3 "" H 2150 3100 50  0001 C CNN
	1    2150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3000 2150 3100
Text GLabel 1550 1650 0    50   Input ~ 0
USB_RESET
$Comp
L Device:C_Small C?
U 1 1 5F84D574
P 5650 6150
AR Path="/5F84D574" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F84D574" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D574" Ref="C25"  Part="1" 
F 0 "C25" V 5879 6150 50  0000 C CNN
F 1 "100 nF" V 5788 6150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5650 6150 50  0001 C CNN
F 3 "~" H 5650 6150 50  0001 C CNN
	1    5650 6150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F84D57A
P 5800 6150
AR Path="/5F84D57A" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F84D57A" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D57A" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 5800 5900 50  0001 C CNN
F 1 "GND" H 5805 5977 50  0000 C CNN
F 2 "" H 5800 6150 50  0001 C CNN
F 3 "" H 5800 6150 50  0001 C CNN
	1    5800 6150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 6150 5800 6150
Text Label 5550 6150 2    50   ~ 0
USB_3V3_OUT
$Comp
L Device:C_Small C?
U 1 1 5F84D588
P 4500 5550
AR Path="/5F84D588" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F84D588" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D588" Ref="C23"  Part="1" 
F 0 "C23" V 4729 5550 50  0000 C CNN
F 1 "4.7 uF" V 4638 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 5550 50  0001 C CNN
F 3 "~" H 4500 5550 50  0001 C CNN
	1    4500 5550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F84D58E
P 4700 5650
AR Path="/5F84D58E" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F84D58E" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D58E" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 4700 5400 50  0001 C CNN
F 1 "GND" H 4705 5477 50  0000 C CNN
F 2 "" H 4700 5650 50  0001 C CNN
F 3 "" H 4700 5650 50  0001 C CNN
	1    4700 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5F84D595
P 4200 5150
AR Path="/5F84D595" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F84D595" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D595" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 4200 5000 50  0001 C CNN
F 1 "+3.3V" V 4200 5350 28  0000 C CNN
F 2 "" H 4200 5150 50  0000 C CNN
F 3 "" H 4200 5150 50  0000 C CNN
	1    4200 5150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F84D5A5
P 4500 5150
AR Path="/5F84D5A5" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F84D5A5" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F84D5A5" Ref="C22"  Part="1" 
F 0 "C22" V 4729 5150 50  0000 C CNN
F 1 "100 nF" V 4638 5150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 5150 50  0001 C CNN
F 3 "~" H 4500 5150 50  0001 C CNN
	1    4500 5150
	0    1    1    0   
$EndComp
$Comp
L Connector:USB_B_Micro J1
U 1 1 5F8569C6
P 7500 1450
F 0 "J1" H 7557 1917 50  0000 C CNN
F 1 "USB_B_Micro" H 7557 1826 50  0000 C CNN
F 2 "" H 7650 1400 50  0001 C CNN
F 3 "~" H 7650 1400 50  0001 C CNN
	1    7500 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R9
U 1 1 5F856E81
P 8300 1750
F 0 "R9" V 8095 1750 50  0000 C CNN
F 1 "22" V 8186 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8300 1750 50  0001 C CNN
F 3 "~" H 8300 1750 50  0001 C CNN
	1    8300 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R10
U 1 1 5F8576D2
P 8300 2050
F 0 "R10" V 8095 2050 50  0000 C CNN
F 1 "22" V 8186 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8300 2050 50  0001 C CNN
F 3 "~" H 8300 2050 50  0001 C CNN
	1    8300 2050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R11
U 1 1 5F857B91
P 8300 2350
F 0 "R11" V 8095 2350 50  0000 C CNN
F 1 "560" V 8186 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8300 2350 50  0001 C CNN
F 3 "~" H 8300 2350 50  0001 C CNN
	1    8300 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 1750 8500 1750
Wire Wire Line
	8400 2050 8500 2050
Wire Wire Line
	8400 2350 8500 2350
Text GLabel 8600 900  2    50   Input ~ 0
USB_VBUS
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 5F862B7A
P 8350 900
F 0 "JP1" H 8350 1105 50  0000 C CNN
F 1 "SJ_VBUS" H 8350 1014 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8350 900 50  0001 C CNN
F 3 "~" H 8350 900 50  0001 C CNN
	1    8350 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 900  8600 900 
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5F866303
P 8950 1250
F 0 "FB1" V 8713 1250 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 8804 1250 50  0000 C CNN
F 2 "" V 8880 1250 50  0001 C CNN
F 3 "~" H 8950 1250 50  0001 C CNN
	1    8950 1250
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D3
U 1 1 5F867059
P 8300 1250
F 0 "D3" H 8300 1034 50  0000 C CNN
F 1 "D_Schottky" H 8300 1125 50  0000 C CNN
F 2 "" H 8300 1250 50  0001 C CNN
F 3 "~" H 8300 1250 50  0001 C CNN
	1    8300 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8450 1250 8850 1250
$Comp
L power:+5V #PWR0142
U 1 1 5F86E558
P 9200 1250
F 0 "#PWR0142" H 9200 1100 50  0001 C CNN
F 1 "+5V" V 9215 1378 50  0000 L CNN
F 2 "" H 9200 1250 50  0001 C CNN
F 3 "" H 9200 1250 50  0001 C CNN
	1    9200 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 1250 9200 1250
Wire Wire Line
	7800 1450 8150 1450
Wire Wire Line
	8150 1450 8150 1750
Wire Wire Line
	8150 1750 8200 1750
Wire Wire Line
	7800 1550 8100 1550
Wire Wire Line
	8100 1550 8100 2050
Wire Wire Line
	8100 2050 8200 2050
Wire Wire Line
	7800 1650 8050 1650
Wire Wire Line
	8050 1650 8050 2350
Wire Wire Line
	8050 2350 8200 2350
Text Notes 6850 700  0    50   ~ 0
USB Serial Port
Wire Notes Line
	6800 2600 6800 600 
Wire Wire Line
	7800 1250 7950 1250
Wire Wire Line
	8200 900  7950 900 
Wire Wire Line
	7950 900  7950 1250
Connection ~ 7950 1250
Wire Wire Line
	7950 1250 8150 1250
Text GLabel 8500 1750 2    50   Input ~ 0
USB_D+
Text GLabel 8500 2050 2    50   Input ~ 0
USB_D-
Text GLabel 8500 2350 2    50   Input ~ 0
USB_ID
Wire Notes Line
	6800 750  7500 750 
Wire Notes Line
	7500 750  7500 600 
Text GLabel 1550 1450 0    50   Input ~ 0
USB_D+
Text GLabel 1550 1550 0    50   Input ~ 0
USB_D-
Wire Wire Line
	4400 5150 4300 5150
Wire Wire Line
	4300 5150 4300 5550
Wire Wire Line
	4700 5150 4600 5150
Wire Wire Line
	4200 5150 4300 5150
Connection ~ 4300 5150
Wire Wire Line
	4700 5650 4700 5550
Wire Wire Line
	4400 5550 4300 5550
Wire Wire Line
	4600 5550 4700 5550
Wire Wire Line
	4700 5550 4700 5150
$Comp
L power:GND #PWR?
U 1 1 5F8F5310
P 4700 6100
AR Path="/5F8F5310" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F8F5310" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F8F5310" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 4700 5850 50  0001 C CNN
F 1 "GND" H 4705 5927 50  0000 C CNN
F 2 "" H 4700 6100 50  0001 C CNN
F 3 "" H 4700 6100 50  0001 C CNN
	1    4700 6100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5F8F5316
P 4200 6000
AR Path="/5F8F5316" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F8F5316" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F8F5316" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 4200 5850 50  0001 C CNN
F 1 "+3.3V" V 4200 6200 28  0000 C CNN
F 2 "" H 4200 6000 50  0000 C CNN
F 3 "" H 4200 6000 50  0000 C CNN
	1    4200 6000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F8F531C
P 4500 6000
AR Path="/5F8F531C" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F8F531C" Ref="C?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F8F531C" Ref="C24"  Part="1" 
F 0 "C24" V 4729 6000 50  0000 C CNN
F 1 "100 nF" V 4638 6000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 6000 50  0001 C CNN
F 3 "~" H 4500 6000 50  0001 C CNN
	1    4500 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 6000 4600 6000
Wire Wire Line
	4200 6000 4300 6000
Wire Wire Line
	4700 6100 4700 6000
Text Label 4150 6150 2    50   ~ 0
USB_VCC
Text Label 4150 5550 2    50   ~ 0
USB_LOGIC
Wire Wire Line
	4150 5550 4300 5550
Connection ~ 4300 5550
Connection ~ 4700 5550
Text GLabel 2650 2050 2    50   Input ~ 0
RTS#
Text GLabel 1550 2250 0    50   Input ~ 0
CTS#
Wire Notes Line
	3700 2700 3700 4700
Text Label 1550 2750 2    50   ~ 0
TX_LED
Text Label 1550 2650 2    50   ~ 0
RX_LED
Wire Notes Line
	3700 2700 6700 2700
Wire Notes Line
	3700 4700 6700 4700
Wire Notes Line
	6700 2700 6700 4700
$Comp
L power:GND #PWR?
U 1 1 5F962789
P 2450 3100
AR Path="/5F962789" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F962789" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5F962789" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 2450 2850 50  0001 C CNN
F 1 "GND" H 2455 2927 50  0000 C CNN
F 2 "" H 2450 3100 50  0001 C CNN
F 3 "" H 2450 3100 50  0001 C CNN
	1    2450 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3000 2450 3100
$Comp
L Device:LED_ALT D1
U 1 1 5F96A000
P 3900 1600
F 0 "D1" V 3939 1482 50  0000 R CNN
F 1 "LED_ALT" V 3848 1482 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3900 1600 50  0001 C CNN
F 3 "~" H 3900 1600 50  0001 C CNN
	1    3900 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED_ALT D2
U 1 1 5F96A006
P 4450 1600
F 0 "D2" V 4489 1482 50  0000 R CNN
F 1 "LED_ALT" V 4398 1482 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4450 1600 50  0001 C CNN
F 3 "~" H 4450 1600 50  0001 C CNN
	1    4450 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R7
U 1 1 5F96A00C
P 3900 2000
F 0 "R7" H 3968 2046 50  0000 L CNN
F 1 "270" H 3968 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3900 2000 50  0001 C CNN
F 3 "~" H 3900 2000 50  0001 C CNN
	1    3900 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R8
U 1 1 5F96A012
P 4450 2000
F 0 "R8" H 4518 2046 50  0000 L CNN
F 1 "270" H 4518 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4450 2000 50  0001 C CNN
F 3 "~" H 4450 2000 50  0001 C CNN
	1    4450 2000
	1    0    0    -1  
$EndComp
Text Label 3900 1300 1    50   ~ 0
USB_LOGIC
Text Label 4450 1300 1    50   ~ 0
USB_LOGIC
Wire Wire Line
	3900 1300 3900 1450
Wire Wire Line
	3900 1750 3900 1900
Wire Wire Line
	3900 2100 3900 2250
Wire Wire Line
	4450 2100 4450 2250
Wire Wire Line
	4450 1750 4450 1900
Text Label 3900 2250 3    50   ~ 0
TX_LED
Text Label 4450 2250 3    50   ~ 0
RX_LED
Wire Wire Line
	4450 1300 4450 1450
Wire Notes Line
	3700 600  3700 2600
Text Notes 3750 700  0    50   ~ 0
LED Interface
Wire Notes Line
	4300 600  4300 750 
Wire Notes Line
	4300 750  3700 750 
Wire Notes Line
	3700 600  6700 600 
Wire Notes Line
	3700 2600 6700 2600
Wire Notes Line
	6700 600  6700 2600
Text Notes 4950 2450 0    50   ~ 0
The FT232R has 3 configuration options \nfor driving LEDs from the CBUS pins.\n\nWhen data is transmitted or received,\nthe respective pins will drive from\ntristate to low in order to provide \nindication on the LEDs of data transfer.\n\n\nTX_LED - Transmit data LED drive:\nData from USB Host to FT232R. \nPulses low when transmitting data via USB. \n\nRX_LED - Receive data LED drive: \nData fromFT232R to USB Host.\nPulses low when receiving data via USB.\n\nThese options can be configured in the \ninternal EEPROM using the software \nutility FT_PROG, an FTDI utility available\non their website.
Text GLabel 1550 2150 0    50   Input ~ 0
DCD#
Text GLabel 1550 2050 0    50   Input ~ 0
DSR#
Text GLabel 2650 1750 2    50   Input ~ 0
12MHz
Text GLabel 1550 1750 0    50   Input ~ 0
12MHz
Text GLabel 1550 1950 0    50   Input ~ 0
RI#
Text GLabel 4350 3200 0    50   Input ~ 0
RX
Text GLabel 4350 4450 0    50   Input ~ 0
CTS#
Text GLabel 4350 4350 0    50   Input ~ 0
DCD#
Text GLabel 4350 4250 0    50   Input ~ 0
DSR#
Text GLabel 4350 4150 0    50   Input ~ 0
RI#
Text GLabel 4350 3100 0    50   Input ~ 0
TX
Text GLabel 4350 3850 0    50   Input ~ 0
RTS#
Text GLabel 2650 1950 2    50   Input ~ 0
DTR#
NoConn ~ 1550 2350
NoConn ~ 1550 2450
Text GLabel 4350 3750 0    50   Input ~ 0
DTR#
Text Notes 3750 2800 0    50   ~ 0
UART Interface Pins
Wire Notes Line
	4550 2850 4550 2700
Wire Notes Line
	3700 2850 4550 2850
NoConn ~ 4350 3100
NoConn ~ 4350 3200
NoConn ~ 4350 3850
NoConn ~ 4350 4150
NoConn ~ 4350 4250
NoConn ~ 4350 4350
NoConn ~ 4350 4450
NoConn ~ 4350 3750
Text Notes 4550 4450 0    50   ~ 0
\n\nTXD - Transmit Asynchronous Data Output\nRXD - Receiving Asynchronous Data Input\n\n\n\nHandshake Signals\n\n\nDTR# - Data Terminal Ready Control Output\nRTS# - Request to Send Control Output\n\n\n\nRI# - Ring Indicator Control Input\nDSR# - Data Set Ready Control Input\nDCD# - Data Carrier Detect Control Input\nCTS# - Clear to Send Control Input
NoConn ~ 1550 2550
Wire Notes Line
	3700 4800 6700 4800
Text Notes 3750 4900 0    50   ~ 0
VCC Power Inputs & Bypass Capacitors
Wire Wire Line
	4150 6150 4300 6150
Wire Wire Line
	4300 6000 4300 6150
Wire Wire Line
	4300 6000 4400 6000
Connection ~ 4300 6000
Text Notes 5000 5700 0    50   ~ 0
Minimum operating voltage VCC must\nbe +4.0V while using the internal clock \ngenerator. Can use VBUS (+5V) to \npower VCC. \n\nOperation at +3.3V is possible when\nusing an external crystal oscillator.
Wire Notes Line
	3700 4800 3700 6400
Wire Notes Line
	3700 6400 6700 6400
Wire Notes Line
	6700 6400 6700 4800
Wire Notes Line
	5300 4950 5300 4800
Wire Notes Line
	3700 4950 5300 4950
Connection ~ 2450 3000
Wire Notes Line
	9800 600  9800 2600
Wire Notes Line
	6800 600  9800 600 
Wire Notes Line
	6800 2600 9800 2600
Wire Wire Line
	7500 2000 7450 2000
Wire Wire Line
	7500 1850 7500 2000
Wire Wire Line
	7400 1850 7400 2000
$Comp
L power:GND #PWR?
U 1 1 5FA100AC
P 7450 2100
AR Path="/5FA100AC" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5FA100AC" Ref="#PWR?"  Part="1" 
AR Path="/60193DF4/5F83AC9F/5FA100AC" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 7450 1850 50  0001 C CNN
F 1 "GND" H 7455 1927 50  0000 C CNN
F 2 "" H 7450 2100 50  0001 C CNN
F 3 "" H 7450 2100 50  0001 C CNN
	1    7450 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2100 7450 2000
Connection ~ 7450 2000
Wire Wire Line
	7450 2000 7400 2000
Wire Notes Line
	600  600  3600 600 
Text Notes 650  700  0    50   ~ 0
FT232 USB2Serial
Wire Notes Line
	600  750  1400 750 
Wire Notes Line
	1400 750  1400 600 
Text Notes 900  4900 0    50   ~ 0
The LM5109 is a  low cost high voltage gate driver,\ndesigned to drive both the high side and the low side\nN-Channel MOSFETs in a synchronous buck or a\nhalf bridge configuration. The floating high-side driver\nis capable of working with rail voltages up to 100V.\n\nThe outputs are independently controlled with TTL\ncompatible input thresholds. A robust level shifter\ntechnology operates at high speed while consuming\nlow power and providing clean level transitions from\nthe control input logic to the high side gate driver.\n\nUnder-voltage lockout is provided on both the low\nside and the high side power rails.
Text Notes 900  3700 0    118  ~ 0
Description
Wire Notes Line
	3350 3500 850  3500
Wire Notes Line
	850  5000 3350 5000
Wire Notes Line
	3350 5000 3350 3500
Wire Notes Line
	850  3500 850  5000
Wire Notes Line
	600  5200 3600 5200
Wire Notes Line
	3600 600  3600 5200
Wire Notes Line
	600  600  600  5200
$EndSCHEMATC
